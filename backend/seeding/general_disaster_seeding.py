import os
import sys
import json
import requests
import psycopg2
import config

STATES = [
    "Alabama",
    "Alaska",
    "Arizona",
    "Arkansas",
    "California",
    "Colorado",
    "Connecticut",
    "Delaware",
    "Florida",
    "Georgia_(U.S._state)",
    "Hawaii",
    "Idaho",
    "Illinois",
    "Indiana",
    "Iowa",
    "Kansas",
    "Kentucky",
    "Louisiana",
    "Maine",
    "Maryland",
    "Massachusetts",
    "Michigan",
    "Minnesota",
    "Mississippi",
    "Missouri",
    "Montana",
    "Nebraska",
    "Nevada",
    "New_Hampshire",
    "New_Jersey",
    "New_Mexico",
    "New_York_(state)",
    "North_Carolina",
    "North_Dakota",
    "Ohio",
    "Oklahoma",
    "Oregon",
    "Pennsylvania",
    "Rhode_Island",
    "South_Carolina",
    "South_Dakota",
    "Tennessee",
    "Texas",
    "Utah",
    "Vermont",
    "Virginia",
    "Washington_(state)",
    "West_Virginia",
    "Wisconsin",
    "Wyoming",
]


def init_table():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()

    try:
        cur.execute("DROP TABLE states;")
        print("table dropped!")
    except Exception as e:
        print(e)

    conn.commit()

    commands = """
          CREATE TABLE states (
              code VARCHAR(2) PRIMARY KEY,
              name VARCHAR(255) NOT NULL,
              capital VARCHAR(255) NOT NULL,
              population VARCHAR(255) NOT NULL,
              area VARCHAR(255) NOT NULL,
              population_density VARCHAR(255) NOT NULL,
              flag VARCHAR(255) NOT NULL,
              map VARCHAR(255) NOT NULL
          );
          """

    try:
        cur.execute(commands)
        print("table created!")
    except Exception as e:
        print(e)

    conn.commit()
    conn.close()


# Retrieves images of the flag and map for a state
def get_images(s, title):

    if " " in s:
        state = s.split(" ")
        s = "_".join(state)

    flag = "Flag_of_" + s
    usmap = s + "_in_United_States"

    imgs = wikipedia.WikipediaPage(title).images
    saved = ["", ""]
    counter = 0

    for i in imgs:
        if flag in i:
            saved[0] = i
            counter += 1
        elif usmap in i:
            saved[1] = i
            counter += 1
        if counter == 2:
            break

    return saved


# Retrieves data for every disaster
def get_all_disasters():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()

    for s in STATES:
        url = (
            "https://en.wikipedia.org/w/api.php?action=parse&page="
            + s
            + "&format=json&prop=wikitext"
        )

        # Make request from api
        response = requests.get(url)

        if response.status_code == 200:
            page = response.json()
            raw = page["parse"]["wikitext"]["*"]
            lines = raw.split("\n")

            # Collect data from api response and clean keys
            state = {}
            for i in range(75):
                info = lines[i].split("=")
                if len(info) > 1:
                    info[0] = clean_key(info[0])
                    state[info[0]] = info[1]

            # Use keys for specific attributes and clean data
            n = clean_data(state["Name"].split(" "), "name")
            name = " ".join(n)

            capital = clean_data(state["Capital"].split(" "), "cap")
            abbrv = clean_data(state["PostalAbbreviation"].split(" "), "abbrv")
            pop = clean_data(state["2010Pop"].split(" "))

            area = clean_data(state["TotalAreaUS"].split(" "), "area")
            if name == "Nevada":
                a = state["TotalAreaUS"].split(" ")
                area = a[8]
                area = area[5 : len(area) - 2]

            dens = ""
            if state.get("2000DensityUS"):
                dens = clean_data(state["2000DensityUS"].split(" "), "dens")
            elif state.get("2010DensityUS"):
                dens = clean_data(state["2010DensityUS"].split(" "), "dens")

            # climate = get_climate(s)
            images = get_images(name, s)

            attributes = (abbrv, name, capital, pop, area, dens, images[0], images[1])
            try:
                sql = """
                  INSERT INTO states(code, name, capital, population, area, population_density, flag, map) VALUES (%s, %s, %s, %s, %s, %s, %s, %s);
                  """
                cur.execute(sql, attributes)
            except Exception as e:
                print(e)

        else:
            print("Retrieving information about countries failed.")

    conn.commit()
    conn.close()


# Clean keys used for state attributes
def clean_key(key):
    cleaned = ""
    for char in key:
        if char != " " and char != "|":
            cleaned += char
    return cleaned


# Clean data used for state attributes
def clean_data(data, datatype=None):
    counter = 0
    name = []
    capital = ""

    for elem in data:
        if elem != "":

            # Capital may be more than one word
            if datatype == "cap":
                if elem[len(elem) - 1] == "," or elem[len(elem) - 1] == "]":
                    if capital == "":
                        capital += elem[2 : len(elem) - 1]
                    else:
                        capital += elem[: len(elem) - 1]

                    if capital[len(capital) - 1] == "]":
                        capital = capital[: len(capital) - 1]

                    return capital
                else:
                    if capital == "":
                        capital += elem[2:] + " "
                    else:
                        capital += elem + " "

            # Area may have extra on it, only accept digits and ","
            elif datatype == "area":
                num = ""
                for digit in elem:
                    if digit.isdigit() or digit == ",":
                        num += digit
                    else:
                        return num
                return num

            # Name may be more than one word
            elif datatype == "name":
                counter += 1
                name.append(elem)
                if counter == 2:
                    return name

            # Abbreviation may have to be cleaned, keep only 2 chars
            elif datatype == "abbrv":
                return elem[:2]

            # Population Density may need to be cleaned
            elif datatype == "dens":
                el = elem.split("|")
                return el[0]

            else:
                return elem

    return name


# init_table()
get_all_disasters()
