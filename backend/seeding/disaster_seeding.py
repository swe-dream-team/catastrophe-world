import os
import sys
import json
import requests
import psycopg2
from img import get_image

DISASTER_URL = "https://www.fema.gov/api/open/v1/FemaWebDisasterDeclarations"
SUMMARY_URL = "https://www.fema.gov/api/open/v1/FemaWebDisasterSummaries?$skip=1500"


def init_table():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    try:
        t = """
            DROP TABLE disasters;
            """
        cur.execute(t)
        print("table dropped!")
    except Exception as e:
        print(e)
    conn.commit()

    commands = (
        """
        CREATE TABLE disasters_info (
            disasterNumber VARCHAR(24) PRIMARY KEY,
            declarationType VARCHAR(255),
            declarationDate DATE,
            disasterName VARCHAR(255),
            stateCode VARCHAR(2),
            stateName VARCHAR(255),
            incidentType VARCHAR(255)
            )
        """,
        """
        CREATE TABLE summary (
            disasterNumber VARCHAR(24) PRIMARY KEY,
            obligatedFunding DECIMAL(12),
            approvedFunding DECIMAL(12)
            )
        """,
    )

    try:
        for c in commands:
            cur.execute(c)
        print("tables created!")
    except Exception as e:
        print(e)

    conn.commit()
    conn.close()


def get_disasters():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    response = requests.get(DISASTER_URL)
    attributes = []
    disasters = json.loads(response.text)["FemaWebDisasterDeclarations"]
    for elem in disasters:
        if response.status_code == 200:
            attributes.append(elem["disasterNumber"])
            attributes.append(elem["declarationType"])
            attributes.append(elem["declarationDate"])
            attributes.append(elem["disasterName"])
            attributes.append(elem["stateCode"])
            attributes.append(elem["stateName"].strip())
            attributes.append(elem["incidentType"])

            attr = tuple(attributes)
            try:
                sql = """
                    INSERT INTO disasters_info(disasterNumber, declarationType, declarationDate, disasterName, stateCode, stateName, incidentType) VALUES (%s, %s, %s, %s, %s, %s, %s)
                    """
                cur.execute(sql, attr)
                attributes = []
            except Exception as e:
                print(e)
        else:
            print("Retrieving information about disasters failed.")
            return None

    conn.commit()
    conn.close()


def get_summary():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    response = requests.get(SUMMARY_URL)
    attributes = []
    disasters = json.loads(response.text)["FemaWebDisasterSummaries"]
    for elem in disasters:
        if response.status_code == 200:
            attributes.append(elem["disasterNumber"])
            if "totalObligatedAmountPa" in elem:
                attributes.append(elem["totalObligatedAmountPa"])
            else:
                attributes.append(0)

            if "totalAmountHaApproved" in elem:
                attributes.append(elem["totalAmountHaApproved"])
            else:
                attributes.append(0)

            attr = tuple(attributes)
            try:
                sql = """
                    INSERT INTO summary(disasterNumber, obligatedFunding, approvedFunding) VALUES (%s, %s, %s)
                    """
                cur.execute(sql, attr)
                attributes = []
            except Exception as e:
                print(e)
        else:
            print("Retrieving information about disaster summaries failed.")
            return None

    conn.commit()
    conn.close()


def merge_tables():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    try:
        sql = (
            """
        create table disasters as (select * from disasters_info INNER JOIN summary using (disasternumber));
        """,
            """
        ALTER TABLE disasters ADD PRIMARY KEY (disasternumber);
        """,
            """
        DROP TABLE disasters_info;
        """,
            """
        DROP TABLE summary;
        """,
        )
        for command in sql:
            cur.execute(command)
        print("merge tables!")
        conn.commit()
    except Exception as e:
        print(e)

    conn.close()


def add_image():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    # try:
    #     sql = (
    #         """
    #         ALTER TABLE disasters ADD image TEXT;
    #         """
    #     )
    #     cur.execute(sql)
    #     print("added image column")
    #     conn.commit()
    # except Exception as e:
    #     print(e)

    try:
        select = """
            SELECT disastername, statename, disasternumber from disasters;
            """
        print("Collected data on disasters")
    except Exception as e:
        print(e)

    cur.execute(select)
    data = cur.fetchall()

    for disaster in data:
        image = get_image(disaster[0], disaster[1].strip())
        disasternumber = disaster[2]
        image_url = image[0]
        try:
            query = """
                UPDATE disasters SET image=%s where disasternumber=%s;
                """
            params = (image_url, disasternumber)
            cur.execute(query, params)
            conn.commit()
        except Exception as e:
            print(e)

    conn.close()


# init_table()
# get_disasters()
# get_summary()
# merge_tables()
# add_image()
