import os
import sys
import json
import requests
import psycopg2
import googleapiclient.discovery

os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

api_service_name = "youtube"
api_version = "v3"
DEVELOPER_KEY = "AIzaSyCIXgnkfjTGL_m6H7jXC7144-HA1WBwEQU"

CHARITY_NAV_URL = "https://api.data.charitynavigator.org/v2/"
CN_APP_ID = "2e4cccf9"
CN_APP_KEY = "498445100b36ad8278fda56671e75f89"

GUIDESTAR_URL = "https://apidata.guidestar.org/premier/v1/"
GS_KEY = "0e7c95ebb0c648a1923fa98b9d9afd56"

querys = ("habitat for humanity", "red cross", "food bank")


def init_table():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    try:
        tables = (
            """
      DROP TABLE organizations_cn;
      """,
            """
      DROP TABLE organizations_gs;
      """,
        )
        for t in tables:
            cur.execute(t)
        print("table dropped!")
    except Exception as e:
        print(e)
    conn.commit()

    commands = (
        """
        CREATE TABLE organizations_cn (
            ein VARCHAR(10) PRIMARY KEY,
            charityName VARCHAR(255) NOT NULL,
            stateOrProvince VARCHAR(255) NOT NULL,
            tagLine VARCHAR(255),
            websiteURL VARCHAR(255),
            image VARCHAR(255)
            )
        """,
        """
        CREATE TABLE organizations_gs (
            ein VARCHAR(10) PRIMARY KEY,
            year VARCHAR(4) NOT NULL,
            mission TEXT,
            social_media TEXT
            )
        """,
    )
    try:
        for c in commands:
            cur.execute(c)
        print("tables created!")
    except Exception as e:
        print(e)

    conn.commit()
    conn.close()


def get_organizations_cn():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    for search in querys:
        url = (
            CHARITY_NAV_URL
            + "Organizations?"
            + "app_id="
            + CN_APP_ID
            + "&app_key="
            + CN_APP_KEY
            + "&search="
            + search.replace(" ", "%20")
            + "&searchType=NAME_ONLY"
        )
        print(url)

        response = requests.get(url)
        if response.status_code == 200:
            attributes = []
            for elem in json.loads(response.text):
                if elem.get("currentRating"):
                    ein = elem["ein"]
                    ein = ein[:2] + "-" + ein[2:]
                    attributes.append(ein)
                    attributes.append(elem["charityName"])
                    attributes.append(elem["mailingAddress"]["stateOrProvince"])
                    if elem["tagLine"]:
                        attributes.append(elem["tagLine"])
                    else:
                        attributes.append("none")

                    if elem["websiteURL"]:
                        attributes.append(elem["websiteURL"])
                    else:
                        attributes.append("none")

                    if elem["cause"]["image"]:
                        attributes.append(elem["cause"]["image"])
                    else:
                        attributes.append("none")

                    attr = tuple(attributes)
                    try:
                        sql = """
              INSERT INTO organizations_cn(ein, charityName, stateOrProvince, tagLine, websiteURL, image) VALUES (%s, %s, %s, %s, %s, %s);
              """
                        cur.execute(sql, attr)
                        attributes = []
                    except Exception as e:
                        print(e)
        else:
            print("Retrieving information about organizations failed.")
            return None

    conn.commit()
    conn.close()


def get_organizations_gs():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    try:
        sql = """
      select ein from organizations_cn;
      """
    except Exception as e:
        print(e)
    cur.execute(sql)
    eins = cur.fetchall()

    # change range to avoid 429 error
    for i in range(28, 38):
        e = eins[i]
        ein = str(e).replace(",", "").strip("()").replace("'", "")
        url = GUIDESTAR_URL + ein
        headers = {"Subscription-Key": GS_KEY}
        response = requests.get(url, headers=headers)

        res_json = json.loads(response.text)
        print(res_json["code"])
        if res_json["code"] == 200:
            attributes = []
            for elem in res_json:
                if elem == "data":
                    summary = res_json["data"]["summary"]
                    attributes.append(summary["ein"])
                    attributes.append(summary["year_founded"])
                    attributes.append(summary["mission"])
                    attributes.append(summary["social_media_urls"])
                    attr = tuple(attributes)

                    try:
                        sql = """
              INSERT INTO organizations_gs(ein, year, mission, social_media) VALUES (%s, %s, %s, %s);
              """
                        cur.execute(sql, attr)
                        conn.commit()
                        print(attributes)
                        attributes = []
                    except Exception as e:
                        print(e)
        elif res_json["code"] == 429:
            pass

    conn.close()


def merge_tables():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()
    try:
        sql = (
            """
      create table organizations as (select * from organizations_cn INNER JOIN organizations_gs using (ein));
      """,
            """
      DROP TABLE organizations_cn;
      """,
            """
      DROP TABLE organizations_gs
      """,
            """
      ALTER TABLE organizations ADD PRIMARY KEY (ein);
      """,
            """
      ALTER TABLE organizations ADD video TEXT;
      """,
        )
        cur.execute(sql)
        print("created table organizations")
        print("delete two previous tables")
        conn.commit()
    except Exception as e:
        print(e)

    conn.close()


def getVideoLink(iD):
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY
    )

    request = youtube.videos().list(part="player", id=iD)
    response = request.execute()

    return response["items"][0]["player"]["embedHtml"].split(" ")[3].split("=")[1]


def getVideoID(orgName):
    youtube = googleapiclient.discovery.build(
        api_service_name, api_version, developerKey=DEVELOPER_KEY
    )

    request = youtube.search().list(
        part="snippet",
        q=orgName,
        type="video",
        videoDuration="short",
        videoEmbeddable="true",
    )
    response = request.execute()

    return response["items"][0]["id"]["videoId"]


def add_videos():
    conn = psycopg2.connect(
        database="catastrophe_db",
        user="catastrophe",
        password="dotworld",
        host="catastrophe-world.chky3jbmtp10.us-east-2.rds.amazonaws.com",
        port="5432",
    )
    cur = conn.cursor()

    try:
        select = """
        SELECT charityname from organizations;
        """
        print("Collected data on organizations")
    except Exception as e:
        print(e)

    cur.execute(select)
    data = cur.fetchall()

    for charity in data:
        iD = getVideoID(charity)
        link = getVideoLink(iD)
        link = link[1 : len(link) - 1]
        try:
            insert = """
        UPDATE organizations SET video=%s WHERE charityname=%s;
        """
            values = (link, charity)
            cur.execute(insert, values)
            conn.commit()
            print("added video link to " + str(charity))
        except Exception as e:
            print(e)

    conn.close()


# init_table()
# get_organizations_cn()
# get_organizations_gs()
# merge_tables()
# add_videos()
