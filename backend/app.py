#!flask/bin/python
from flask import Flask, request
import json
import sqlalchemy
import os
import sys
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
CORS(app)

from sql import Query

query = Query(app)

state_params = (
    "page",
    "per_page",
    "statecode",
    "stateorprovince",
    "sortby",
    "pop_min",
    "pop_max",
    "area_min",
    "area_max",
    "region",
    "searchTerm",
)
disaster_params = (
    "page",
    "per_page",
    "statecode",
    "stateorprovince",
    "sortby",
    "funding",
    "incident",
    "searchTerm",
)
org_params = (
    "page",
    "per_page",
    "statecode",
    "stateorprovince",
    "sortby",
    "rating",
    "year",
    "searchTerm",
)


@app.route("/")
def get_landing_page():
    return "This is the Catastrophe.World API"


@app.route("/states/")
def get_states():
    query_params = {key: request.args.get(key) for key in state_params}
    return query.get_data("states", query_params)


@app.route("/natural-disasters/")
def get_natural_disasters():
    query_params = {key: request.args.get(key) for key in disaster_params}
    return query.get_data("disasters", query_params)


@app.route("/organizations/")
def get_organizations():
    query_params = {key: request.args.get(key) for key in org_params}
    return query.get_data("organizations", query_params)


@app.route("/states/<code>")
def get_specific_state(code):
    return query.get_instance_data("states", code)


@app.route("/organizations/<ein>")
def get_specific_organization(ein):
    return query.get_instance_data("organizations", ein)


@app.route("/natural-disasters/<disasternumber>")
def get_specific_disaster(disasternumber):
    return query.get_instance_data("disasters", disasternumber)


@app.route("/search/")
def get_search():
    return query.search(request.args.get("search"))


if __name__ == "__main__":
    app.run(debug=True)
