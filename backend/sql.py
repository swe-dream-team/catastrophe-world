#!flask/bin/python
from flask import Flask
import json
import sqlalchemy
from sqlalchemy import or_
import os
import sys
from flask_sqlalchemy import SQLAlchemy


class Query:
    def __init__(self, app):
        self.app = app
        app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_DATABASE_URI"]
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

        db = SQLAlchemy(app)
        self.db = db

        db.Model.metadata.reflect(db.engine)

        class Organizations(db.Model):
            __table__ = db.Model.metadata.tables["organizations"]

            def serialize(self):
                return {
                    "ein": "{}".format(self.ein),
                    "charityname": "{}".format(self.charityname),
                    "stateorprovince": "{}".format(self.stateorprovince),
                    "tagline": "{}".format(self.tagline),
                    "websiteurl": "{}".format(self.websiteurl),
                    "image": "{}".format(self.image),
                    "year": "{}".format(self.year),
                    "mission": "{}".format(self.mission),
                    "social_media": "{}".format(self.social_media),
                    "video": "{}".format(self.video),
                    "rating": "{}".format(self.rating),
                }

        class Disasters(db.Model):
            __table__ = db.Model.metadata.tables["disasters"]

            def serialize(self):
                return {
                    "disasternumber": "{}".format(self.disasternumber),
                    "declarationtype": "{}".format(self.declarationtype),
                    "declarationdate": "{}".format(self.declarationdate),
                    "disastername": "{}".format(self.disastername),
                    "statecode": "{}".format(self.statecode),
                    "statename": "{}".format(self.statename),
                    "incidenttype": "{}".format(self.incidenttype),
                    "obligatedfunding": "{}".format(self.obligatedfunding),
                    "approvedfunding": "{}".format(self.approvedfunding),
                    "image": "{}".format(self.image),
                }

        class States(db.Model):
            __table__ = db.Model.metadata.tables["states"]

            def serialize(self):
                return {
                    "code": "{}".format(self.code),
                    "name": "{}".format(self.name),
                    "capital": "{}".format(self.capital),
                    "population": "{}".format(self.population),
                    "area": "{}".format(self.area),
                    "population_density": "{}".format(self.population_density),
                    "flag": "{}".format(self.flag),
                    "map": "{}".format(self.map),
                    "climate": "{}".format(self.climate),
                    "region": "{}".format(self.region),
                }

        self.organizations = Organizations
        self.disasters = Disasters
        self.states = States

    def get_data(self, request, params):
        if request == "organizations":
            model = self.organizations
            qry = model.query

            if params.get("searchTerm"):
                attributes = [
                    model.charityname,
                    model.tagline,
                    model.stateorprovince,
                    model.websiteurl,
                    model.mission,
                ]
                search = "%" + params["searchTerm"].lower() + "%"
                qry = qry.filter(or_(attr.ilike(search) for attr in attributes))

            if params.get("stateorprovince"):
                qry = qry.filter_by(stateorprovince=params["stateorprovince"])

            if params.get("rating"):
                qry = qry.filter(model.rating == int(params["rating"]))

            if params.get("year"):
                first_year = int(params["year"].split("-")[0])
                sec_year = params["year"].split("-")[1]
                qry = qry.filter(
                    model.yearfounded >= first_year, model.yearfounded < sec_year
                )

            if params.get("sortby"):
                asc = "asc" in params["sortby"]
                sort_val = params["sortby"].split("-")[1]
                if asc:
                    if sort_val == "name":
                        qry = qry.order_by(model.charityname)
                    elif sort_val == "state":
                        qry = qry.order_by(model.stateorprovince)
                else:
                    if sort_val == "name":
                        qry = qry.order_by(model.charityname.desc())
                    elif sort_val == "state":
                        qry = qry.order_by(model.stateorprovince.desc())

        elif request == "disasters":
            model = self.disasters
            qry = model.query

            if params.get("searchTerm"):
                attributes = [model.disastername, model.incidenttype, model.statename]
                search = "%" + params["searchTerm"].lower() + "%"
                qry = qry.filter(or_(attr.ilike(search) for attr in attributes))

            if params.get("statecode"):
                qry = qry.filter_by(statecode=params["statecode"])

            if params.get("incident"):
                qry = qry.filter_by(incidenttype=params["incident"])

            if params.get("funding"):
                qry = qry.filter(model.obligatedfunding >= int(params["funding"]))

            if params.get("sortby"):
                asc = "asc" in params["sortby"]
                sort_val = params["sortby"].split("-")[1]
                if asc:
                    if sort_val == "type":
                        qry = qry.order_by(model.incidenttype)
                    elif sort_val == "state":
                        qry = qry.order_by(model.statename)
                else:
                    if sort_val == "type":
                        qry = qry.order_by(model.incidenttype.desc())
                    elif sort_val == "state":
                        qry = qry.order_by(model.statename.desc())

        elif request == "states":
            model = self.states
            qry = model.query

            if params.get("searchTerm"):
                attributes = [
                    model.name,
                    model.code,
                    model.region,
                    model.climate,
                    model.capital,
                ]
                search = "%" + params["searchTerm"].lower() + "%"
                qry = qry.filter(or_(attr.ilike(search) for attr in attributes))

            if params.get("pop_min"):
                qry = qry.filter(model.population >= int(params["pop_min"]))
            if params.get("pop_max"):
                qry = qry.filter(model.population <= int(params["pop_max"]))

            if params.get("area_min"):
                qry = qry.filter(model.area >= int(params["area_min"]))
            if params.get("area_max"):
                qry = qry.filter(model.area <= int(params["area_max"]))

            if params.get("region"):
                qry = qry.filter_by(region=params["region"])

            if params.get("sortby"):
                asc = "asc" in params["sortby"]
                sort_val = params["sortby"].split("-")[1]
                if asc:
                    if sort_val == "name":
                        qry = qry.order_by(model.name)
                    elif sort_val == "population":
                        qry = qry.order_by(model.population)
                    elif sort_val == "area":
                        qry = qry.order_by(model.area)
                else:
                    if sort_val == "name":
                        qry = qry.order_by(model.name.desc())
                    elif sort_val == "population":
                        qry = qry.order_by(model.population.desc())
                    elif sort_val == "area":
                        qry = qry.order_by(model.area.desc())

        page, per_page = 1, 100
        if params["page"] and params["per_page"]:
            page, per_page = int(params["page"]), int(params["per_page"])

        page = qry.paginate(page, per_page, error_out=True)
        data = [m.serialize() for m in page.items]
        return json.dumps(data, indent=2)

    def get_instance_data(self, model, id):
        if model == "organizations":
            model = self.organizations
            data = model.query.filter_by(ein=id).first()
        elif model == "disasters":
            model = self.disasters
            data = model.query.filter_by(disasternumber=id).first()
        elif model == "states":
            model = self.states
            data = model.query.filter_by(code=id).first()

        return json.dumps(data.serialize(), indent=2)

    def search(self, search):
        res = []
        term = "%" + search.lower() + "%"

        model = self.states
        state_qry = model.query

        attributes = [model.name, model.code, model.region, model.climate]
        state_qry = state_qry.filter(or_(attr.ilike(term) for attr in attributes))

        model = self.disasters
        dis_qry = model.query

        attributes = [
            model.disastername,
            model.incidenttype,
            model.statename,
            model.statecode,
        ]
        dis_qry = dis_qry.filter(or_(attr.ilike(term) for attr in attributes))

        model = self.organizations
        org_qry = model.query

        attributes = [
            model.charityname,
            model.tagline,
            model.stateorprovince,
            model.websiteurl,
            model.mission,
        ]
        org_qry = org_qry.filter(or_(attr.ilike(term) for attr in attributes))

        page, per_page = 1, 100
        queries = [state_qry, dis_qry, org_qry]

        for qry in queries:
            pg = qry.paginate(page, per_page, error_out=True)
            data = [m.serialize() for m in pg.items]
            res.append(data)
        return json.dumps(res, indent=2)
