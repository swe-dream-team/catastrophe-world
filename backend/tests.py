# Chandler: 0
# Preston: 5
# Allen: 5
# David: 5
# Alex: 17

from unittest import main, TestCase
from app import (
    get_landing_page,
    get_states,
    get_organizations,
    get_natural_disasters,
    get_specific_state,
    get_specific_organization,
    get_specific_disaster,
)
import requests


class TestBackend(TestCase):
    # Alex
    def test_1(self):
        assert get_landing_page() == "This is the Catastrophe.World API"

    # Preston
    def test_2(self):
        URL_states = "https://api.catastrophe.world/states/"
        PARAMS = {"page": 1, "per_page": 50}
        result_states = requests.get(url=URL_states, params=PARAMS).json()
        assert len(result_states) == 50

    # Preston
    def test_3(self):
        URL_natural_disasters = "https://api.catastrophe.world/natural-disasters/"
        PARAMS = {"page": 1, "per_page": 50}
        result_natural_disasters = requests.get(
            url=URL_natural_disasters, params=PARAMS
        ).json()
        assert len(result_natural_disasters) == 50

    # Preston
    def test_4(self):
        URL_organizations = "https://api.catastrophe.world/organizations/"
        PARAMS = {"page": 1, "per_page": 50}
        result_organizations = requests.get(url=URL_organizations, params=PARAMS).json()
        assert len(result_organizations) == 35

    # Preston
    def test_5(self):
        URL_TX = "https://api.catastrophe.world/states/TX"
        result_TX = requests.get(url=URL_TX).json()
        assert len(result_TX) == 10

    # Preston
    def test_6(self):
        URL_TX = "https://api.catastrophe.world/states/TX"
        result_TX = requests.get(url=URL_TX).json()
        assert result_TX["population"] == "28701845"

    # Allen
    def test_7(self):
        URL_HI = "https://api.catastrophe.world/states/HI"
        result_HI = requests.get(URL_HI).json()
        assert len(result_HI) == 10

    # Allen
    def test_8(self):
        URL_HI = "https://api.catastrophe.world/states/HI"
        result_HI = requests.get(URL_HI).json()
        assert result_HI["population"] == "1420491"

    # Allen
    def test_9(self):
        URL_NATE = "https://api.catastrophe.world/natural-disasters/3392"
        result_NATE = requests.get(url=URL_NATE).json()
        assert len(result_NATE) == 10

    # Allen
    def test_10(self):
        URL_NATE = "https://api.catastrophe.world/natural-disasters/3392"
        result_NATE = requests.get(url=URL_NATE).json()
        assert result_NATE["statecode"] == "LA"

    # Allen
    def test_11(self):
        URL_SEVERE_STORM = "https://api.catastrophe.world/natural-disasters/4326"
        result_SEVERE_STORM = requests.get(url=URL_SEVERE_STORM).json()
        assert len(result_SEVERE_STORM) == 10

    # Alex
    def test_12(self):
        URL_SEVERE_STORM = "https://api.catastrophe.world/natural-disasters/4326"
        result_SEVERE_STORM = requests.get(url=URL_SEVERE_STORM).json()
        assert result_SEVERE_STORM["statecode"] == "MI"

    # Alex
    def test_13(self):
        URL_HBT_PENINSULA = "https://api.catastrophe.world/organizations/94-3088881"
        result_HBT_PENINSULA = requests.get(url=URL_HBT_PENINSULA).json()
        assert len(result_HBT_PENINSULA) == 11

    # Alex
    def test_14(self):
        URL_HBT_PENINSULA = "https://api.catastrophe.world/organizations/94-3088881"
        result_HBT_PENINSULA = requests.get(url=URL_HBT_PENINSULA).json()
        assert result_HBT_PENINSULA["stateorprovince"] == "CA"

    # Alex
    def test_15(self):
        URL_HBT_TAMPA = "https://api.catastrophe.world/organizations/59-2850410"
        result_HBT_TAMPA = requests.get(url=URL_HBT_TAMPA).json()
        assert len(result_HBT_TAMPA) == 11

    # Alex
    def test_16(self):
        URL_HBT_TAMPA = "https://api.catastrophe.world/organizations/59-2850410"
        result_HBT_TAMPA = requests.get(url=URL_HBT_TAMPA).json()
        assert result_HBT_TAMPA["stateorprovince"] == "FL"

    # David sort
    def test_17(self):
        URL_STATES_ASC_POP = (
            "https://api.catastrophe.world/states/?sortby=asc-population"
        )
        result_URL_STATES_ASC_POP = requests.get(url=URL_STATES_ASC_POP).json()
        assert result_URL_STATES_ASC_POP[0]["code"] == "WY"

    # David sort
    def test_18(self):
        URL_STATES_DESC_POP = (
            "https://api.catastrophe.world/states/?sortby=desc-population"
        )
        result_URL_STATES_DESC_POP = requests.get(url=URL_STATES_DESC_POP).json()
        assert result_URL_STATES_DESC_POP[0]["code"] == "CA"

    # David sort
    def test_19(self):
        URL_STATES_ASC_NAME = "https://api.catastrophe.world/states/?sortby=asc-name"
        result_URL_STATES_ASC_NAME = requests.get(url=URL_STATES_ASC_NAME).json()
        assert result_URL_STATES_ASC_NAME[0]["capital"] == "Montgomery"

    # David sort
    def test_20(self):
        URL_STATES_DESC_NAME = "https://api.catastrophe.world/states/?sortby=desc-name"
        result_URL_STATES_DESC_NAME = requests.get(url=URL_STATES_DESC_NAME).json()
        assert result_URL_STATES_DESC_NAME[1]["capital"] == "Madison"

    # Alex sort
    def test_21(self):
        URL_ND_ASC_STATE = (
            "https://api.catastrophe.world/natural-disasters/?sortby=asc-state"
        )
        result_URL_ND_ASC_STATE = requests.get(url=URL_ND_ASC_STATE).json()
        assert result_URL_ND_ASC_STATE[1]["disasternumber"] == "3292"

    # Alex sort
    def test_22(self):
        URL_ND_DESC_TYPE = (
            "https://api.catastrophe.world/natural-disasters/?sortby=desc-type"
        )
        result_URL_ND_DESC_TYPE = requests.get(url=URL_ND_DESC_TYPE).json()
        assert result_URL_ND_DESC_TYPE[0]["obligatedfunding"] == "11358962"

    # Alex sort
    def test_23(self):
        URL_ORG_DESC_STATE = (
            "https://api.catastrophe.world/organizations/?sortby=desc-state"
        )
        result_URL_ORG_DESC_STATE = requests.get(url=URL_ORG_DESC_STATE).json()
        assert result_URL_ORG_DESC_STATE[4]["rating"] == "3"

    # Alex sort
    def test_24(self):
        URL_ORG_ASC_TYPE = (
            "https://api.catastrophe.world/organizations/?sortby=asc-type"
        )
        result_URL_ORG_ASC_TYPE = requests.get(url=URL_ORG_ASC_TYPE).json()
        assert result_URL_ORG_ASC_TYPE[1]["year"] == "1989"

    # Alex search
    def test_25(self):
        URL_STATES = "https://api.catastrophe.world/states/?searchTerm=Austin"
        result_search = requests.get(url=URL_STATES).json()
        assert result_search[0]["code"] == "TX"

    # Alex search
    def test_26(self):
        URL_ND = "https://api.catastrophe.world/natural-disasters/?searchTerm=Colorado"
        result_search = requests.get(url=URL_ND).json()
        assert result_search[1]["incidenttype"] == "Fire"

    # Alex search
    def test_27(self):
        URL_ORG = "https://api.catastrophe.world/organizations/?searchTerm=food"
        result_search = requests.get(url=URL_ORG).json()
        assert result_search[0]["stateorprovince"] == "OR"

    # Alex filter
    def test_28(self):
        URL_POP = (
            "https://api.catastrophe.world/states/?pop_min=2000000&pop_max=4350000"
        )
        result_pop = requests.get(url=URL_POP).json()
        assert result_pop[2]["population_density"] == "34.9"

    # Alex filter
    def test_29(self):
        URL_AREA = "https://api.catastrophe.world/states/?area_min=50000&area_max=78000"
        result_area = requests.get(url=URL_AREA).json()
        assert result_area[2]["population_density"] == "165.0"

    # Alex filter
    def test_30(self):
        URL_REGION = "https://api.catastrophe.world/states/?region=Southwest"
        result_region = requests.get(url=URL_REGION).json()
        assert result_region[2]["population_density"] == "17.2"

    # Alex filter
    def test_31(self):
        URL_FUNDING = (
            "https://api.catastrophe.world/natural-disasters/?funding=100000000"
        )
        result_funding = requests.get(url=URL_FUNDING).json()
        assert result_funding[1]["incidenttype"] == "Flood"

    # Alex filter
    def test_32(self):
        URL_STATECODE = "https://api.catastrophe.world/natural-disasters/?statecode=FL"
        result_statecode = requests.get(url=URL_STATECODE).json()
        assert result_statecode[2]["incidenttype"] == "Fire"

    # Alex filter
    def test_33(self):
        URL_STATECODE = "https://api.catastrophe.world/natural-disasters/?statecode=FL"
        result_statecode = requests.get(url=URL_STATECODE).json()
        assert result_statecode[2]["incidenttype"] == "Fire"

    # Alex filter
    def test_34(self):
        URL_INCIDENT = (
            "https://api.catastrophe.world/natural-disasters/?incident=Tornado"
        )
        result_incident = requests.get(url=URL_INCIDENT).json()
        assert result_incident[0]["statecode"] == "IL"

    # Alex filter
    def test_35(self):
        URL_YEAR = "https://api.catastrophe.world/organizations/?year=1940-1980"
        result_year = requests.get(url=URL_YEAR).json()
        assert result_year[0]["stateorprovince"] == "GA"

    # Alex filter
    def test_36(self):
        URL_STATEORPROVINCE = (
            "https://api.catastrophe.world/organizations/?stateorprovince=TX"
        )
        result_stateorprovince = requests.get(url=URL_STATEORPROVINCE).json()
        assert result_stateorprovince[2]["rating"] == "4"

    # Alex filter
    def test_37(self):
        URL_RATING = "https://api.catastrophe.world/organizations/?rating=3"
        result_rating = requests.get(url=URL_RATING).json()
        assert result_rating[1]["year"] == "1989"


if __name__ == "__main__":
    main()
