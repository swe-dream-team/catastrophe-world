# catastrophe-world

## Proposal
Our goal is to promote relief for victims of natural disasters throughout the world and spread awareness to
different organizations that work for these victims. Furthermore, we hope to educate our audience about different natural disasters and the locations they occur in. 
Millions of people are affected by natural disasters, but we only often hear about the ones that happen near us. 
Our objective is to support the recovery efforts and provide some relief to victims of these catastrophes.

## API URLs
Charity Navigator: https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1397 <br />
Wikipedia: https://www.mediawiki.org/wiki/API:Main_page <br />
GuideStar: https://learn.guidestar.org/products/business-solutions/guidestar-apis <br />
FEMA: https://www.fema.gov/openfema-api-documentation <br />

## Models
States: attributes include population, area, population density, capital, flag, and natural disasters that have occured in the state

Natural Disasters: attributes include incident type, location, date, funding, and photos/video of incident

Organizations: attributes include state of headquarter location, description, website, social media links, and founding year 

## Note
We do not have a “frontend” folder. Our main frontend file is “App.js”. We do have an “index.js”, but it only renders “App.js”. Both of these files are located in the “src” directory. We do have a “backend” folder, but our main backend file is “app.py”, not “main.py”.

## Visualizations
Our visualizations are located on the "Visualizations" dropdown tab on the nav bar.

## Estimated Time to Completion

User Story 1: Est .5 hour; Actual 1 hour <br />
User Story 2: Est 20 minutes; Actual .5 hours<br />
User Story 3: Est 3 hours; Actual 12 hours<br />
User Story 4: Est 1 hour; Actual 2 hours<br />
User Story 5: Est 1 hour; Actual 1 hour<br />

Estimated Time to Complete Phase IV: 34 hours <br />
Actual Time to Complete Phase IV:  36 hours <br />

## GIT SHA
1515c56f5114433499660d73f9d4379e0ee11d72
