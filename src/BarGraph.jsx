import React, { Component } from 'react';
import * as d3 from 'd3';
import axios from 'axios';
import './css/BarGraph.css';

//Bar graph visualization for UnbEATable Food
class BarGraph extends Component {

  constructor(props) {
	    super(props);
	    this.state = {
	      isLoaded: false,
	      items: {}
	    };
      this.create = this.create.bind(this);
      this.getLocationData = this.getLocationData.bind(this);
	}

	create () {
		const sample = [
		  {
		    language: 'HEB 41st Street',
		    value: this.state.items["HEB 41st Street"]
		  },
		  {
		    language: 'Wheatsville Food Co-op',
		    value: this.state.items["Wheatsville Food Co-op"]
		  },
		  {
		    language: 'Fresh Plus Grocery',
		    value: this.state.items["Fresh Plus Grocery"]
		  },
		  {
		    language: 'Fiesta Mart ',
		    value: this.state.items["Fiesta Mart "]
		  },
		  {
		    language: 'West Campus Market',
		    value: this.state.items["West Campus Market"]
		  },
		  {
		    language: 'Central Market',
		    value: this.state.items["Central Market"]
		  },
		  {
		    language: 'Speedway Market',
		    value: this.state.items["Speedway Market"]
		  },
		  {
		    language: 'Royal Blue Grocery - Congress',
		    value: this.state.items["Royal Blue Grocery - Congress"]
		  },
		  {
		    language: "Kin's Market",
		    value: this.state.items["Kin's Market"]
		  },
		  {
		    language: 'Royal Blue Grocery- Brazos ',
		    value: this.state.items["Royal Blue Grocery- Brazos "]
		  },
      {
        language: 'Red River Market',
        value: this.state.items["Red River Market"]
      },
      {
        language: "Trader Joe's ",
        value: this.state.items["Trader Joe's "]
      },
      {
        language: 'Whole Foods Market',
        value: this.state.items["Whole Foods Market"]
      },
      {
        language: 'The Bee Grocery ',
        value: this.state.items["The Bee Grocery "]
      },
      {
        language: 'Rio Market',
        value: this.state.items["Rio Market"]
      },
      {
        language: 'Bodega on Rio ',
        value: this.state.items["Bodega on Rio "]
      },
      {
        language: 'Fresh Plus Grocery - Lynn',
        value: this.state.items["Fresh Plus Grocery - Lynn"]
      },
      {
        language: 'New Speedway Grocery ',
        value: this.state.items["New Speedway Grocery "]
      },
      {
        language: 'Randalls ',
        value: this.state.items["Randalls "]
      },
      {
        language: 'Fresh Plus Grocery - 43rd',
        value: this.state.items["Fresh Plus Grocery - 43rd"]
      },
		];

	    const svg = d3.select('#bar-graph-loc');
	    const svgContainer = d3.select('#container-loc');

	    const margin = 80;
	    const width = 1000 - 2 * margin;
	    const height = 600 - 2 * margin;

	    const chart = svg.append('g')
	      .attr('transform', `translate(${margin}, ${margin})`);

	    const xScale = d3.scaleBand()
	      .range([0, width])
	      .domain(sample.map((s) => s.language))
	      .padding(0.4)

	    const yScale = d3.scaleLinear()
	      .range([height, 0])
	      .domain([0, 30]);

	    const makeYLines = () => d3.axisLeft()
	      .scale(yScale)

	    chart.append('g')
	      .attr('transform', `translate(0, ${height})`)
	      .call(d3.axisBottom(xScale))
        .selectAll('text')
          .attr("transform", "rotate(90)")
          .style("text-anchor", "start")
          .style("fill", "white")
          .style("font-size", "15px");

	    chart.append('g')
	      .call(d3.axisLeft(yScale))
        .selectAll('text')
          .style("fill", "white")
          .style("font-size", "15px");

	    chart.append('g')
	      .attr('class', 'grid')
	      .call(makeYLines()
	        .tickSize(-width, 0, 0)
	        .tickFormat('')
	      )

	    const barGroups = chart.selectAll()
	      .data(sample)
	      .enter()
	      .append('g')

	    barGroups
	      .append('rect')
	      .attr('class', 'bar')
	      .attr('x', (g) => xScale(g.language))
	      .attr('y', (g) => yScale(g.value))
	      .attr('height', (g) => height - yScale(g.value))
	      .attr('width', xScale.bandwidth())

	    barGroups
	      .append('text')
	      .attr('class', 'value')
	      .attr('x', (a) => xScale(a.language) + xScale.bandwidth() / 2)
	      .attr('y', (a) => yScale(a.value) + 30)
	      .attr('text-anchor', 'middle')

	    svg
	      .append('text')
	      .attr('class', 'label')
	      .attr('x', -(height / 2) - margin)
	      .attr('y', margin / 2.4)
	      .attr('transform', 'rotate(-90)')
	      .attr('text-anchor', 'middle')
	      .text('Number of Food Items')
          .style("font-size", "20px");

	    // svg.append('text')
	    //   .attr('class', 'label')
	    //   .attr('x', width / 2 + margin)
	    //   .attr('y', height + margin * 1.7)
	    //   .attr('text-anchor', 'middle')
	    //   .text('Locations')
      //     .style("font-size", "20px");

	}

  getLocationData() {
    let items_loc = {};
    for(var i = 1; i <= 20; i++){
      axios.get("https://cors.io/?https://www.unbeatable-food.com:5000/api/location/" + i).then(response => {
        var name = response.data["location"].name;
        var numFoods = response.data["items"].length;
        items_loc[name] = 0;
        items_loc[name] += numFoods;
        this.forceUpdate();
      });
    }
    this.setState({isLoaded: true, items: items_loc});
  }

  componentDidUpdate() {
    this.create();
  }

  componentDidMount() {
    this.getLocationData();
  }

  render() {
    return (
      <div id='container-loc'>
          <svg id="bar-graph-loc"></svg>
      </div>
    );
  }

}

export default BarGraph;
