// Chandler: 6
// Preston: 10
// Allen: 0
// David: 5
// Alex: 0

import React from 'react';
import ReactDOM from 'react-dom';
import { configure, shallow, mount } from 'enzyme';
import { expect } from 'chai'
import Adapter from "enzyme-adapter-react-16"
import About from '../src/About';
import App from '../src/App';
import Navbar from '../src/Navbar';
import Carousel from '../src/Carousel';
import Organizations from '../src/Organizations';
import States from '../src/States';
import NaturalDisasters from '../src/NaturalDisasters';

import OrganizationInstance from '../src/components/OrganizationInstance';
import StateInstance from '../src/components/StateInstance';
import DisasterInstance from '../src/components/DisasterInstance';
import OrganizationCard from '../src/components/OrganizationCard';
import StatesCard from '../src/components/StatesCard';
import DisasterCard from '../src/components/DisasterCard';

configure({ adapter: new Adapter() });

describe('Navigation', () => {

  it('should verify navbar', () => {
    const wrapper = shallow(<Navbar />);
    expect(wrapper.find('.navbar-brand').length).to.equal(1);
  });
  it('should verify title', () => {
  	const wrapper = shallow(<Navbar />);
    expect(wrapper.find('.navbar-brand').text()).to.equal(" catastrophe.world");
  });

  it('should render correct number of tabs', () => {
    const wrapper = shallow(<Navbar />);
    expect(wrapper.find('.nav-item').length).to.equal(5);
  })
});


describe('About', () => {
  it('should count separate containers', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('container').length).to.equal(0);
  });
  it('should count links correctly', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('links').length).to.equal(0);
  })
  it('should count profiles correctly', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('testimonials').length).to.equal(0);
  })
});

describe('Carousel', () => {
  it('should verify carousel', () => {
    const wrapper = shallow(<Carousel />);
    expect(wrapper.find('.container-fluid').length).to.equal(1);
  });

});

describe('Organizations', () => {
  it('should verify organization', () => {
    const wrapper = shallow(<Organizations />);
    expect(wrapper.find('totalOrgs').length).to.equal(0);
  });
});

describe('Natural Disasters', () => {
  it('should verify Natural Disaster card is loaded', () => {
    const wrapper = shallow(<NaturalDisasters />);
    expect(wrapper.find('totalDisasters').length).to.equal(0);
  });
});

describe('States',  () => {
  it('should verify correct amount of states', () => {
    const wrapper = shallow(<States />);
    expect(wrapper.find('totalStates').length).to.equal(0);
  });
});

// New Tests Chandler

async function getData(type) {
    try {
        const response = await axios.get("api.catastrophe.world/" + type + "?page=1&per_page=1000");
        return response.data;
    } catch(error) {
        return error;
    }
}


describe('States Card',  () => {
  it('should verify correct amount of states', async () => {
    await mount(<States />);
    const data = await getData("states");
    expect(data.length).to.not.equal(0);
  });
  it('verifies the information for the States Card are loaded', async () => {
    await mount(<States />);
    const data = await getData("states");
    expect(data["name"].length).to.not.equal(0);
  });
});


describe('Natural Disasters Card', () => {
  it('should verify Natural Disaster Card loads properly', () => {
    const wrapper = shallow(<DisasterCard />);
    expect(wrapper.find('card-title').length).to.equal(0);
  });
  it('verifies the information for the Natural Disaster Card is loaded', () => {
    const wrapper = shallow(<DisasterCard />);
    expect(wrapper.find('card-subtitile').length).to.equal(0);
  });
});


describe('Organizations Card', () => {
  it('should verify the organization card is loaded', async () => {
    await mount(<Organizations />);
    const data = await getData("organizations");
    expect(data.length).to.not.equal(0);
  });
  it('verifies the information for the Organization Card is loaded', async () => {
    await mount(<Organizations />);
    const data = await getData("organizations");
    expect(data["name"].length).to.not.equal(0);
  });
});

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});
