import React, { Component } from 'react';
import axios from 'axios';
import Select from 'react-select';
import Pagination from './components/Pagination';
import OrganizationCard from './components/OrganizationCard';
import Footer from './components/Footer';
import 'react-widgets/dist/css/react-widgets.css';
import load from './img/final.gif';
import './css/Organizations.css'

// Organizations model page
class Organizations extends Component {
  per_page = 12;

  states = [
    { value: '', label: 'All States' },
    { value: 'AL', label: 'Alabama' }, { value: 'AK', label: 'Alaska' }, { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' }, { value: 'CA', label: 'California' }, { value: 'CO', label: 'Colorado' },
    { value: 'CT', label: 'Connecticut' }, { value: 'DE', label: 'Delaware' }, { value: 'FL', label: 'Florida' },
    { value: 'GA', label: 'Georgia' }, { value: 'HI', label: 'Hawaii' }, { value: 'ID', label: 'Idaho' },
    { value: 'IL', label: 'Illinois' }, { value: 'IN', label: 'Indiana' }, { value: 'IA', label: 'Iowa' },
    { value: 'KS', label: 'Kansas' }, { value: 'KY', label: 'Kentucky' }, { value: 'LA', label: 'Louisiana' },
    { value: 'MI', label: 'Michigan' }, { value: 'MD', label: 'Maryland' }, { value: 'MA', label: 'Massachusetts' },
    { value: 'MO', label: 'Missouri' }, { value: 'MN', label: 'Minnesota' }, { value: 'MS', label: 'Mississippi' },
    { value: 'MT', label: 'Montana' }, { value: 'NE', label: 'Nebraska' }, { value: 'NV', label: 'Nevada' },
    { value: 'NH', label: 'New Hampshire' }, { value: 'NJ', label: 'New Jersey' }, { value: 'NM', label: 'New Mexico' },
    { value: 'NY', label: 'New York' }, { value: 'NC', label: 'North Carolina' }, { value: 'ND', label: 'North Dakota' },
    { value: 'OH', label: 'Ohio' }, { value: 'OK', label: 'Oklahoma' }, { value: 'OR', label: 'Oregon' },
    { value: 'PA', label: 'Pennsylvania' }, { value: 'RI', label: 'Rhode Island' }, { value: 'SC', label: 'South Carolina' },
    { value: 'SD', label: 'South Dakota' }, { value: 'TN', label: 'Tennessee' }, { value: 'TX', label: 'Texas' },
    { value: 'UT', label: 'Utah' }, { value: 'VT', label: 'Vermont' }, { value: 'VA', label: 'Virginia' },
    { value: 'WA', label: 'Washington' }, { value: 'WV', label: 'West Virginia' }, { value: 'WI', label: 'Wisconsin' },
    { value: 'WY', label: 'Wyoming' }
  ];

  sortOptions = [
    { value: '', label: 'Default' },
    { value: 'asc-name', label: 'Name A-Z' },
    { value: 'dsc-name', label: 'Name Z-A' },
    { value: 'asc-state', label: 'State A-Z' },
    { value: 'dsc-state', label: 'State Z-A' },
  ];

  ratings = [
    { value: '', label: 'All Ratings'},
    { value: '1', label: '\u2605'},
    { value: '2', label: '\u2605\u2605'},
    { value: '3', label: '\u2605\u2605\u2605'},
    { value: '4', label: '\u2605\u2605\u2605\u2605'}
  ]

  years = [
    {value: '', label: 'All Years'},
    {value: '0-1900', label: '< 1900'},
    {value: '1900-1940', label: '1900 - 1940'},
    {value: '1940-1980', label: '1940 - 1980'},
    {value: '1980-2000', label: '1980 - 2000'},
    {value: '2000-2019', label: '> 2000'}
  ]

  constructor(props) {
    super(props);
    this.state = {
        orgs: 35,
        allOrgs: [],
        currentOrgs: [],
        currentPage: 1,
        totalPages: null,
        sortby: '',
        location: '',
        rating: '',
        searchTerm: '',
        year: '',
        isLoaded: false
    };
    this.onSort = this.onSort.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.onRatingFilter = this.onRatingFilter.bind(this);
    this.searchUpdated = this.searchUpdated.bind(this);
    this.onYearFilter = this.onYearFilter.bind(this);
    this.highlighterWords = this.highlighterWords.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
  }

  buildUrl(currentPage){
    // let url = "http://localhost:5000/organizations/?page=" + currentPage
    let url = "https://cors.io/?https://api.catastrophe.world/organizations/?page=" + currentPage
    + "&per_page=" + this.per_page
    + "&sortby=" + this.state.sortby
    + "&stateorprovince=" + this.state.location
    + "&rating=" + this.state.rating
    + "&year=" + this.state.year
    + "&searchTerm=" + this.state.searchTerm;

    console.log(url)
    return url;
  }

  getTotalUrl(){
    // let url = "http://localhost:5000/organizations/?"
    let url = "https://cors.io/?https://api.catastrophe.world/organizations/?"
    + "&sortby=" + this.state.sortby
    + "&stateorprovince=" + this.state.location
    + "&rating=" + this.state.rating
    + "&year=" + this.state.year
    + "&searchTerm=" + this.state.searchTerm;

    // console.log(url)
    return url;
  }

  componentDidMount() {
    // axios.get('http://localhost:5000/organizations/').then(response => {
    //    this.setState({ allOrgs: response.data});
    // });

    axios.get('https://cors.io/?https://api.catastrophe.world/organizations/').then(response => {
       this.setState({ allOrgs: response.data, isLoaded: true});
    });
  }

  onPageChanged = data => {
    // const { currentPage, totalPages} = data;
    const { currentPage, totalPages, pageOne, preview} = data;

    var curr;
    if (pageOne) {
      curr = 1;
    }
    else {
      curr = currentPage;
    }

    const url = this.buildUrl(curr)

    axios.get(url).then(response => {
      const currentOrgs = response.data;
      if (preview) {
        this.setOrgs(currentOrgs, this.state.searchTerm);
      }
      this.setState({ currentPage: curr, currentOrgs, totalPages });
   });
  }

  setOrgs(orgs, term) {
    var orgCards = [];

    var len = orgs.length;

    var t = term;
    t = t.replace("+", " ");
    t = t.toLowerCase();

    var i;
    for (i = 0; i < len; i++) {
      orgs[i]["preview"] = this.orgContains(t, orgs[i]);
      orgCards.push(orgs[i]);
    }

    // console.log(stateCards);
    // this.setState({stateLoaded: true});
    this.setState({currentOrgs: orgCards});
  }

  orgContains(term, org) {
    var name = org["charityname"].toLowerCase();
    var code = org["tagline"].toLowerCase();
    var region = org["stateorprovince"].toLowerCase();

    if (name.includes(term)) {
      return false;
    }
    else if (code.includes(term)) {
      return false;
    }
    else if (region.includes(term)) {
      return false;
    }

    return true;
  }

  onSort(e) {
    this.setState(
        {sortby: e.value},
        function () {
            // this.onPageChanged(this.state)
            var data = this.state;
            data.pageOne = true;
            this.onPageChanged(data);
            this.onFilterChange();
        }
    )
  }

  onFilterChange() {
    const url = this.getTotalUrl();
    axios.get(url).then(response => {
      const current = response.data;
      var orgs = current.length;
      this.setState({ orgs });
      // console.log(currentStates);
      // this.setState({ currentPage, current, totalPages });
      // console.log(totalPages);
   });
  }

  onFilter(val){
    var filter_values = {};
    filter_values = {location: val.value}
    this.setState(
      filter_values,
      function() {
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  // filters by rating
  onRatingFilter(val){
    console.log(val);
    var filter_values = {};
    filter_values = {rating: val.value}
    this.setState(
      filter_values,
      function() {
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  // filters by year
  onYearFilter(year){
    var filter_values = {};
    filter_values = {year: year.value}
    this.setState(
      filter_values,
      function() {
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  searchUpdated () {
    this.setState(
      // {searchTerm: term.target.value},
      function (){
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        data.preview = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  updateSearch (term) {
    this.setState({searchTerm: term.target.value});
  }

  highlighterWords() {
    var textChildren = document.getElementById('cards').childNodes;
    var text;
    for (var i = 0; i < textChildren.length; i++){
      text += textChildren[i].innerText;
    }
    return text;
  }

  clearAll(){
    window.location.reload()
  }

  render() {
    const { allOrgs, currentOrgs, currentPage, orgs } = this.state;
    const totalOrgs = allOrgs.length;

    const pages = Math.ceil(orgs/this.per_page);
    const curr = (pages === 0) ? 0 : currentPage;

    if (totalOrgs === 0) return (<div style={{background: '#FCFAFC', height: '100vh'}}>
      <img src={load} alt="loading" style={{marginTop: '5%', width: '20%'}}></img>
    </div>);

    const headerClass = ['text-dark py-2 pr-4 m-0', curr ? 'border-gray border-right' : ''].join(' ').trim();

    return (
      <div>

      <div className="container mb-5">
        <div className="row d-flex flex-row py-5">
          <div className="w-100 px-4 py-5 d-flex flex-row flex-wrap align-items-center justify-content-between">
            <div className="d-flex flex-row align-items-center">
              <h2 className={headerClass}>
                <strong className="text-secondary">{orgs}</strong> Organizations
              </h2>
              { curr && (
                <span className="current-page d-inline-block h-100 pl-4 text-secondary">
                  Page <span className="font-weight-bold">{ curr }</span> / <span className="font-weight-bold">{ pages }</span>
                </span>
              ) }
            </div>
            <button type="button" className="btn btn-danger" onClick={this.clearAll}>Reset Filters</button>
          </div>

          <div style={{textAlign: 'left', fontSize: '120%', fontWeight: 'bold'}}>Filter Options</div>
          {/* first row of filters */}
          <div className="row" id="filter-row-1">
            <div className="col-lg-3 col-md-4">
              <Select name="State Filter" placeholder="State" defaultValue=""
                      options={this.states} onChange={this.onFilter}/>
            </div>
            <div className="col-lg-6 col-md-4"></div>
            <div className="col-lg-3 col-md-4">
              <Select name="Sort" placeholder="Sort Options" defaultValue=""
                      options={this.sortOptions} onChange={this.onSort}/>
            </div>
          </div>

          {/* second row of filters */}
          <div className="row" id="filter-row-2">
            <div className="col-lg-2 col-md-3">
              <Select name="Rating Filter" placeholder="Rating" defaultValue=""
                      options={this.ratings} onChange={this.onRatingFilter}/>
            </div>
            <div className="col-lg-2 col-md-3">
              <Select name="Year Filter" placeholder="Year Founded" defaultValue=""
                      options={this.years} onChange={this.onYearFilter}/>
            </div>
            <div className="col-lg-5 col-md-2"></div>

            {/* search bar */}
            <div className="col-lg-3 col-md-4">
              <input id='searchInput' className="form-control form-control-sm ml-0 w-75" type="text" placeholder="Search" aria-label="Search" style={{height: '37px', display: 'inline'}} onChange={this.updateSearch}>
              </input>
              <button type="submit" style={{border: 'none', background: 'none', padding: '0px', color: 'grey'}} onClick={this.searchUpdated}>
                <i className="fas fa-search" style={{display: 'inline', paddingLeft: '10px', fontSize: '20px'}}></i>
              </button>
            </div>
          </div>
            { currentOrgs.map(organization => <OrganizationCard key={organization.ein} organization={organization} search={this.state.searchTerm.split(" ")} preview={organization.preview} mission={organization.mission}/>) }
        </div>

        {/* pagination */}
        <div className="d-flex flex-row py-4" style={{width: '100%', alignContent: 'center', textAlign: 'center', justifyContent: 'center'}}>
          <Pagination totalRecords={orgs} pageLimit={12} pageNeighbours={1} onPageChanged={this.onPageChanged} currentPage={curr}/>
        </div>
      </div>

      <Footer/>
      </div>
    );

  }
}

export default Organizations;
