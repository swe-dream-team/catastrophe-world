import React from 'react';
import "react-bootstrap-carousel/dist/react-bootstrap-carousel.css";
import Carousel from "react-bootstrap-carousel";

// Carousel for landing page
class DisasterCarousel extends React.PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        autoplay: true
      };
    }
    onSelect = (active, direction) => {
      console.log(`active=${active} && direction=${direction}`);
    };
    visiableOnSelect = active => {
      console.log(`visiable onSelect active=${active}`);
    };
    slideNext = () => {
      this.slider.slideNext();
    };
    slidePrev = () => {
      this.slider.slidePrev();
    };
    autoplay = () => {
      this.setState({ autoplay: !this.state.autoplay });
    };

    render() {
      let { leftIcon, rightIcon } = this.state;
      return (
        <div className="container-fluid">
          <Row>
            <Col>
              <Carousel
                animation={true}
                autoplay={this.state.autoplay}
                leftIcon={leftIcon}
                rightIcon={rightIcon}
                onSelect={this.onSelect}
                ref={r => (this.slider = r)}
                version={4}>

                <div style={{ height: "100vh", width:"100%",
                              background: "url(./img/volcano.jpg) no-repeat scroll center",
                              backgroundSize: "cover" }}>
                  <div className="carousel-center">
                    <h1 style={{ color: 'white' }}>Helping those affected by natural disasters.</h1>
                  </div>
                </div>

                <div style={{ height: "100vh", width:"100%",
                              background: "url(https://media.wired.com/photos/5a9f3dadfc41d94f4aaf3e10/master/pass/23.08.FF_.GlobalRescue.DH_.2015_04_29_DB_Nepal_2146.jpg) no-repeat scroll center",
                              backgroundSize: "cover" }}>
                  <div className="carousel-center">
                    <h1 style={{ color: 'white' }}>Promoting relief efforts.</h1>
                  </div>
                </div>

                <div style={{ height: "100vh" }}>
                  <img alt="tornado"
                    style={{ width: "100%", height: "100%" }}
                    src="https://cff2.earth.com/uploads/2016/10/03153657/tornado-supercell-evening-stock.jpg"
                  />
                  <div className="carousel-center">
                    <h1 style={{ color: 'white' }}>Educating the public.</h1>
                  </div>
                </div>

              </Carousel>
            </Col>
          </Row>
          <a href="#second">
            <div style={{position: "absolute", top: "calc(100vh - 70px)", left: "calc(50vw - 15px)", transform: "rotate(-45deg)", borderLeft: "2px solid lightgrey", borderBottom: "2px solid lightgrey", width: "30px", height: "30px"}}>
            </div>
          </a>
        </div>
      );
    }
  }

// Bootstrap Component
const Row = props => <div className="row">{props.children}</div>;
const Col = props => (
  <div className={`col-${props.span}`} style={props.style}>
    {props.children}
  </div>
);

export default DisasterCarousel;
