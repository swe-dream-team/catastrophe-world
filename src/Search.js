import React, { Component } from 'react';
import axios from 'axios';
import StatesCard from './components/StatesCard';
import OrganizationCard from './components/OrganizationCard';
import DisasterCard from './components/DisasterCard';
import Tabs from './components/Tabs';
import Pagination from './components/Pagination';
import load from './img/final.gif';
import './css/style.css';

// search page that displays search results for site-wide search bar
class Search extends Component {

  constructor(props) {
    super(props);
    this.state = {
        allCards: null,
        search: this.props.location.search,
        stateLoaded: false,
        disLoaded: false,
        orgLoaded: false,
        term: '',
        states: 0,
        disas: 0,
        orgs: 0,
        statePg: 1,
        disPg: 1,
        orgPg: 1
    }

    this.stateCards = [];
    this.disCards = [];
    this.orgCards = [];
  }

  componentDidMount() {
    let url = "https://api.catastrophe.world/search/" + this.state.search;
    // let url = "http://localhost:5000/search/" + this.state.search;
    //console.log(url)
    axios.get(url).then(response => {
      this.setState({allCards: response.data});
      // this.setState({isLoaded: true});
      this.setState({states: response.data[0].length});
      this.setState({disas: response.data[1].length});
      this.setState({orgs: response.data[2].length});
      this.setStates(response.data[0]);
      this.setDis(response.data[1]);
      this.setOrgs(response.data[2]);
    })
  }

  setStates(states) {
    this.stateCards = [];

    var len = states.length;

    if (len === 0 ) {
      this.stateCards.push([]);
    }
    else {
      var t = this.getTerm();
      t = t.replace("+", " ");
      t = t.toLowerCase();

      var i;
      // for (i = 0; i < len; i+=12) {
      //   if (i + 12 > len) {
      //     this.stateCards.push(states.slice(i, len));
      //   }
      //   else {
      //     this.stateCards.push(states.slice(i, i + 12));
      //   }
      // }
      var page = [];
      for (i = 0; i < len; i++) {
        states[i]["preview"] = this.stateContains(t, states[i]);

        if (i % 12 === 0 && i !== 0) {
          this.stateCards.push(page);
          page = [];
        }

        page.push(states[i]);
      }

      if (page.length > 0) {
        this.stateCards.push(page);
      }
    }

    this.setState({stateLoaded: true});
  }

  stateContains(term, state) {
    var name = state["name"].toLowerCase();
    var code = state["code"].toLowerCase();
    var region = state["region"].toLowerCase();

    if (name.includes(term)) {
      return false;
    }
    else if (code.includes(term)) {
      return false;
    }
    else if (region.includes(term)) {
      return false;
    }

    return true;
  }

  setDis(disasters) {
    this.disCards = [];

    var len = disasters.length;

    if (len === 0) {
      this.disCards.push([]);
    }
    else {
      var i;
      for (i = 0; i < len; i+=12) {
        if (i + 12 > len) {
          this.disCards.push(disasters.slice(i, len));
        }
        else {
          this.disCards.push(disasters.slice(i, i + 12));
        }
      }
    }

    this.setState({disLoaded: true});
  }

  setOrgs(orgs) {
    this.orgCards = [];

    var len = orgs.length;

    if (len === 0) {
      this.orgCards.push([]);
    }
    else {
      var t = this.getTerm();
      t = t.replace("+", " ");
      t = t.toLowerCase();

      var i;
      // for (i = 0; i < len; i+=12) {
      //   if (i + 12 > len) {
      //     this.orgCards.push(orgs.slice(i, len));
      //   }
      //   else {
      //     this.orgCards.push(orgs.slice(i, i + 12));
      //   }
      // }

      var page = [];
      for (i = 0; i < len; i++) {
        orgs[i]["preview"] = this.orgContains(t, orgs[i]);

        if (i % 12 === 0 && i !== 0) {
          this.orgCards.push(page);
          page = [];
        }

        page.push(orgs[i]);
      }

      if (page.length > 0) {
        this.orgCards.push(page);
      }
    }

    this.setState({orgLoaded: true});
  }

  orgContains(term, org) {
    var name = org["charityname"].toLowerCase();
    var code = org["tagline"].toLowerCase();
    var region = org["stateorprovince"].toLowerCase();

    if (name.includes(term)) {
      return false;
    }
    else if (code.includes(term)) {
      return false;
    }
    else if (region.includes(term)) {
      return false;
    }

    return true;
  }

  getTerm() {
    var term = this.state.search.split("=")[1];
    return term;
  }

  statePageChanged = data => {
    const { currentPage } = data;
    this.setState({statePg: currentPage});
  }

  disasPageChanged = data => {
    const { currentPage } = data;
    this.setState({disPg: currentPage});
  }

  orgPageChanged = data => {
    const { currentPage } = data;
    this.setState({orgPg: currentPage});
  }

  render() {
    const { states, disas, orgs, statePg, disPg, orgPg, stateLoaded, disLoaded, orgLoaded } = this.state;
    var t = this.getTerm();

    const st = (statePg === 0) ? 1 : statePg;
    const di = (disPg === 0) ? 1 : disPg;
    const or = (orgPg === 0) ? 1 : orgPg;

    if(stateLoaded && disLoaded && orgLoaded) {

      return (
        <div className="container mb-5">
          <div className="row d-flex flex-row py-5">
            <h3 style={{marginTop: '3%', marginBottom: '2%'}} id="search-header">Search results for "{t.split(' ')}"</h3>
            {/* tab divider for three models */}
            <Tabs>
              {/* states tab */}
              <div label="States">
                <h6 style={{textAlign: 'left', marginBottom: '17px'}}>If <mark style={{backgroundColor: 'white', color: 'rgb(254, 208, 76)', padding: '0px'}}>highlighted</mark> text is not shown on card, hover over "View More"</h6>
                <div className="row">
                  { this.stateCards[st-1].map(state => <StatesCard key={state.code} state={state} search={t.split(" ")} preview={state.preview} climate={state.climate}/>)}
                </div>
                <div className="d-flex flex-row py-4" style={{width: '100%', alignContent: 'center', textAlign: 'center', justifyContent: 'center'}}>
                  <Pagination totalRecords={states} pageLimit={12} pageNeighbours={0} onPageChanged={this.statePageChanged} currentPage={statePg}/>
                </div>
              </div>

              {/* natural disasters tab */}
              <div label="Natural Disasters">
                <div className="row">
                  { this.disCards[di-1].map(disaster => <DisasterCard key={disaster.disasternumber} disaster={disaster} search={t.split(" ")} />)}
                </div>
                <div className="d-flex flex-row py-4" style={{width: '100%', alignContent: 'center', textAlign: 'center', justifyContent: 'center'}}>
                  <Pagination totalRecords={disas} pageLimit={12} pageNeighbours={0} onPageChanged={this.disasPageChanged} currentPage={disPg}/>
                </div>
              </div>

              {/* organizations tab */}
              <div label="Organizations">
                <h6 style={{textAlign: 'left', marginBottom: '17px'}}>If <mark style={{backgroundColor: 'white', color: 'rgb(254, 208, 76)', padding: '0px'}}>highlighted</mark> text is not shown on card, hover over "View More"</h6>
                <div className="row">
                  { this.orgCards[or-1].map(organization => <OrganizationCard key={organization.ein} organization={organization} search={t.split(" ")} preview={organization.preview} mission={organization.mission}/>)}
                </div>
                <div className="d-flex flex-row py-4" style={{width: '100%', alignContent: 'center', textAlign: 'center', justifyContent: 'center'}}>
                  <Pagination totalRecords={orgs} pageLimit={12} pageNeighbours={0} onPageChanged={this.orgPageChanged} currentPage={orgPg}/>
                </div>
              </div>
            </Tabs>
          </div>
        </div>
      );
    }
    else{
      return (<div style={{background: '#FCFAFC', height: '100vh'}}>
        <img src={load} alt="loading" style={{marginTop: '5%', width: '20%'}}></img>
        </div>);
    }
  }
}

export default Search;
