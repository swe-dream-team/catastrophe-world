import React, { Component } from 'react'
import './Scrollbar.css'

class Scrollbar extends Component {
  render() {
    return (
      <div className="scrollbar scrollbar-morpheus-den">
        <div className="force-overflow"></div>
      </div>
    );
  }
}

export default Scrollbar
