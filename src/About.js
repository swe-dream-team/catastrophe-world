import React, { Component } from 'react';
import './css/About.css'
import GitLabStats from './GitLabStats';
import Footer from './components/Footer';

class About extends Component {
  render() {
    return (

      <div className="body-padding">

        {/* Purpose */}
        <section className="purpose">
          <div className="row">
            <div className="col-md-6 col-lg-6">
              <img src="./img/helping-hand.png" alt="hands" id="purpose-image"/>
            </div>
            <div className="col-md-6 col-lg-6">
              <h1 id="purpose-header">Purpose</h1>
              <p id="purpose-text" >
              Our goal is to promote awareness and relief for victims of natural disasters throughout the United States.
              We strive to educate our audience about the different types of natural disasters and highlight the various
              organizations that work for these victims. By providing a wide array of information and statistics regarding
              such disasters, we hope to make an impact on all citizens throughout America.
              </p>
            </div>
          </div>
          <a href="#second">
            <div style={{position: "absolute", top: "calc(100vh - 70px)", left: "calc(50vw - 15px)", transform: "rotate(-45deg)", borderLeft: "2px solid grey", borderBottom: "2px solid grey", width: "30px", height: "30px"}}>
            </div>
          </a>
        </section>

       {/* Meet the team */}
        <section id="second" className="testimonials text-center">
        <div className="container">
        <h2 className="infotitles">Meet the Team</h2>

        <div className="row">
            <div className="col-lg-4">
                <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img className="img-fluid rounded-circle mb-3" src="./img/preston.jpg" alt=""></img>
                    <h5>Preston Bao</h5>
                      <p className="mb-0">
                        <b>Full-stack</b> <br />
                        Plano East HS<br />
                        <b>6</b> ft <b>1</b> in | <b>190</b> lbs <br />

                      </p>
                      <p className="font-weight-light mb-0">
                        Trust the process.
                      </p>
                </div>
            </div>
            <div className="col-lg-4">
                <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img className="img-fluid rounded-circle mb-3" src="./img/allen.jpg" alt=""></img>
                    <h5>Allen Wang</h5>
                      <p className="mb-0">
                      <b>Full-stack</b> <br />
                      Westwood HS <br />
                      <b>5</b> ft <b>9</b> in | <b>135</b> lbs <br />
                      </p>
                      <p className="font-weight-light mb-0">
                        Photography, hiking, electronic music, basketball.
                      </p>
                </div>
            </div>
            <div className="col-lg-4">
                <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                    <img className="img-fluid rounded-circle mb-3" src="./img/alex.jpg" alt=""></img>
                    <h5>Alex Leung</h5>
                      <p className="mb-0">
                      <b>Front-end</b><br />
                      Frisco Liberty HS <br/>
                      <b>5</b> ft <b>3</b> in | <b>130</b> lbs <br />
                      </p>
                      <p className="font-weight-light mb-0">
                        Wishes Dallas sports teams were better than they currently are.
                      </p>
                </div>
            </div>
        </div>

        <div className="row">
          <div className="col-lg-2"></div>
          <div className="col-lg-4">
              <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/david.jpg" alt=""></img>
                  <h5>David Shiu</h5>
                    <p className="mb-0">
                    <b>Full-stack</b> <br />
                    Arlington Lamar HS <br />
                    <b>5</b> ft <b>6</b> in | <b>137</b> lbs <br />
                    </p>
                    <p className="font-weight-light mb-0">
                      Catch me at Greg.
                    </p>
              </div>
          </div>
          <div className="col-lg-4">
              <div className="testimonial-item mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/chandler.jpg" alt=""></img>
                  <h5>Chandler Yoon</h5>
                    <p className="mb-0">
                      <b>Front-end</b> <br />
                      Cypress Falls HS <br />
                      <b>6</b> ft <b>3</b> in | <b>170</b> lbs<br />
                    </p>
                    <p className="font-weight-light mb-0">
                      My name is Chandler and I always end my sentences with good sir, good sir.
                    </p>
              </div>
          </div>
          <div className="col-lg-2"></div>
        </div>
        </div>
      </section>

      {/* Gitlab Stats*/}
      <GitLabStats/>

      <div className="information col-xs-4">
        <div className="container" style={{color: 'black', marginBottom: "10vh", marginTop: "4vh"}}>
          {/* Tools */}
          <h2 className="infotitles">Tools</h2>

          <div className="row">

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/aws.jpg" alt=""></img>
                <a href="https://aws.amazon.com/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>Amazon Web Services</b><br /><br />
                      Static web hosting, Database(PostgreSQL) hosting
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/flask.png" alt=""></img>
                <a href="http://flask.pocoo.org/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>Flask</b><br /><br />
                      Python enviornment for API calls
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/gitlab.jpg" alt=""></img>
                <a href="https://about.gitlab.com/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>GitLab</b><br /><br />
                      Version control, User stories/issues
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
              <a href="https://www.sqlalchemy.org/" target="_blank" rel="noopener noreferrer">
                <img className="img-fluid rounded-circle mb-3" src="./img/sqlAlchemy.png" alt=""></img>
                <div className="overlay">
                  <div className="tool-text" style={{fontSize: '100%'}}>
                    <b>SQLAlchemy</b><br /><br />
                    Python SQL toolkit
                  </div>
                </div>
              </a>
              </div>
            </div>

          </div>

          <div className="row">

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/namecheap.png" alt=""></img>
                <a href="https://www.namecheap.com/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>NameCheap</b><br /><br />
                      Domain name, DNS service
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/postman.png" alt=""></img>
                <a href="https://www.getpostman.com/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>Postman</b><br /><br />
                      RESTful API documentation and requests
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/reactBootstrap.png" alt=""></img>
                <a href="https://react-bootstrap.github.io/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>React-Bootstrap</b><br /><br />
                      Front-end framework
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/jest.png" alt=""></img>
                <a href="https://jestjs.io/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>Jest</b><br /><br />
                      JavaScript unit testing
                    </div>
                  </div>
                </a>
              </div>
            </div>

          </div>

          <div className="row">

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/selenium.jpg" alt=""></img>
                <a href="https://www.seleniumhq.org/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>Selenium</b><br /><br />
                      Front-end unit testing, web crawler for images
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/postgresql.png" alt=""></img>
                <a href="https://www.postgresql.org/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>PostgreSQL</b><br /><br />
                      Database
                    </div>
                  </div>
                </a>
              </div>
            </div>

            <div className="col-lg-3 col-md-6">
              <div className="tool-container mx-auto mb-5 mb-lg-0">
                <img className="img-fluid rounded-circle mb-3" src="./img/d3.png" alt=""></img>
                <a href="https://d3js.org/" target="_blank" rel="noopener noreferrer">
                  <div className="overlay">
                    <div className="tool-text" style={{fontSize: '100%'}}>
                      <b>D3</b><br /><br />
                      Visualizations
                    </div>
                  </div>
                </a>
              </div>
            </div>

          </div>

        </div>

          <div className="container text-center">
            {/* Data Sources */}
            <h2 className="infotitles">Data Sources</h2>

            <div className="row" style={{marginBottom: "8vh"}}>

              <div className="col-lg-1"></div>

              <div className="col-lg-2 col-md-3">
                <div className="tool-container-mini mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/charityNavigatorLogo.png" alt=""></img>
                  <a href="https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1397" target="_blank" rel="noopener noreferrer">
                    <div className="overlay">
                      <div className="tool-text" style={{fontSize: '100%'}}>
                        <b>Charity Navigator</b><br /><br />
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div className="tool-container-mini mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/wikipediaLogo.png" alt=""></img>
                  <a href="https://www.mediawiki.org/wiki/API:Main_page" target="_blank" rel="noopener noreferrer">
                    <div className="overlay">
                      <div className="tool-text" style={{fontSize: '100%'}}>
                        <b>Wikipedia</b><br /><br />
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div className="tool-container-mini mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/guidestarLogo.jpg" alt=""></img>
                  <a href="https://learn.guidestar.org/products/business-solutions/guidestar-apis" target="_blank" rel="noopener noreferrer">
                    <div className="overlay">
                      <div className="tool-text" style={{fontSize: '100%'}}>
                        <b>Guidestar</b><br /><br />
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div className="tool-container-mini mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/femaLogo.png" alt=""></img>
                  <a href="https://www.fema.gov/openfema-api-documentation" target="_blank" rel="noopener noreferrer">
                    <div className="overlay">
                      <div className="tool-text" style={{fontSize: '100%'}}>
                        <b>FEMA</b><br /><br />
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-2 col-md-3">
                <div className="tool-container-mini mx-auto mb-5 mb-lg-0">
                  <img className="img-fluid rounded-circle mb-3" src="./img/youtubeLogo.png" alt=""></img>
                  <a href="https://developers.google.com/youtube/v3/" target="_blank" rel="noopener noreferrer">
                    <div className="overlay">
                      <div className="tool-text" style={{fontSize: '100%'}}>
                        <b>YouTube</b><br /><br />
                      </div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="col-lg-1"></div>

            </div>
          </div>

          {/* Links */}
          <div className="links text-center mb-3">
            <h2 className="infotitles">Links</h2>
            <dl className="linklist">
              <dd className="link-item"><a href="https://gitlab.com/swe-dream-team/catastrophe-world/">GitLab</a></dd>
              <dd className="link-item"><a href="https://documenter.getpostman.com/view/5841816/S17wM6DE">Postman API</a></dd>
              <dd className="link-item"><a href="/visualizations">Visualizations</a></dd>
            </dl>
          </div>

        </div>

    <Footer/>
    </div>
    );
  }

}

export default About;
