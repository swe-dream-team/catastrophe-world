import React, { Component } from 'react';
import * as d3 from "d3";
import uStates from './uStates';
import axios from 'axios';
import './Visualizations.css';
import './css/style.css';

// Map visualization for states
class MapVisual extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      items: null
    };
    this.drawChart = this.drawChart.bind(this);
  }

  drawChart() {
    function tooltipHtml(n, d) {
      var htmlTable =  "<h4>"+n+"</h4><table>";
      for(var i in d.incidents){
        htmlTable += "<tr><td>" + (i) + ": <td>" +(d.incidents[i]) + "</td></td></tr>"
      }
      htmlTable +="<tr><td><b>Total</b></td><td><b>"+(d.total)+"</b></td></tr></table>";
      return htmlTable;
    }

    var disasterData = this.state.items;
    var sampleData = {};

    ["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
    "ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH",
    "MI", "WY", "MT", "ID", "WA", "TX", "CA", "AZ", "NV", "UT",
    "CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN",
    "WI", "MO", "AR", "OK", "KS", "LA", "VA"]
      .forEach(function(d){
        var stateDisasters = disasterData[d];
        var num = 0;
        for(var incident in stateDisasters){
          num += stateDisasters[incident];
        }
        sampleData[d]={total: num, incidents: stateDisasters,
            color:d3.interpolate("#eaf2ff", "#00225b")(num/80)};
      });
    /* draw states on id #statesvg */
    uStates.draw("#statesvg", sampleData, tooltipHtml);

    d3.select(window.frameElement).style("height", "600px");
  }

  getDisasterData(){
    let statecounts = {};
    axios.get('https://cors.io/?https://api.catastrophe.world/natural-disasters/?page=1&per_page=1000').then(response => {
      response.data.forEach(disaster => {
      const name = disaster.statecode;
      const type = disaster.incidenttype;
      if (!(name in statecounts)) {
        statecounts[name] = {};
      }
      if (!(type in statecounts[name])){
        statecounts[name][type] = 0;
      }
      statecounts[name][type]++;
    });
    this.setState({isLoaded: true, items: statecounts});
    });
    return statecounts;

  }

  componentDidMount() {
    this.getDisasterData();
  }

  render() {
    if(this.state.isLoaded){
      this.drawChart();
    }
    return (
      <div className="container mb-5">
        <div className="row">
          <div id="tooltip"></div>
          <svg id="statesvg" width="960" height="600" style={{marginTop: '5%'}}></svg>
        </div>
      </div>
    );
  }
}
export default MapVisual;
