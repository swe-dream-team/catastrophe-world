import React, { Component } from 'react';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import * as d3 from 'd3';
import axios from 'axios';
import './Visualizations.css';

// Bubble Visualization for Natural Disasters
class Bubble extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      data: null,
      items: null,
    };
  }

  getDisasterData(){
    let disasters = [];
    let full_info = {};
    axios.get('https://cors.io/?https://api.catastrophe.world/natural-disasters/?page=1&per_page=15&funding=100000000').then(response => {
      response.data.forEach(disaster => {
        const name = disaster.disastername;
        const funding = Math.floor(disaster.obligatedfunding / 1000000);
        const location = disaster.statename;
        const date = disaster.declarationdate;
        const number = disaster.disasternumber;
        // var disasterData = {label: name, value: funding, color: d3.interpolate("#e8ffff", "#61a8a8")(funding/900)};
        var disasterData = {label: name, value: funding, color: d3.interpolatePuBu((funding/900))};
        full_info[name] = {value: funding, ddate: date, dlocation: location, num: number};
        disasters.push(disasterData);
      });
      this.setState({isLoaded: true, data: disasters, items: full_info});
    });

    return disasters;
  }

  componentDidMount() {
    this.getDisasterData();
  }

  render() {
    if(this.state.isLoaded){
      var data = this.state.data;
    }
    var bubbleClick = (label) =>{
      document.getElementById("title").innerHTML = label;
      document.getElementById("subtitle").innerHTML = "";
      var data = this.state.items[label];
      document.getElementById("funding").innerHTML = "Funding given (in millions USD): $" + data['value'];
      document.getElementById("date").innerHTML = "Date: " + data['ddate'];
      document.getElementById("location").innerHTML = "Location: " + data['dlocation'];
      document.getElementById("link").href = "/natural-disasters/" + data['num'];
    }
    return (
      <div className="container">

        <div className="row">

          <div className="card col-lg-3" style={{height: "100%", marginTop: "20vh"}}>
            <div className="card-body" id="bubble-card">
              <h5 className="card-title" id="title">Click on a Bubble</h5>
              <h6 className="card-subtitle mb-2 text-muted" id="subtitle">For more information</h6>
              <ul className="list-group list-group-flush">
                <li className="list-group-item" id="funding">Funding given (in millions USD): </li>
                <li className="list-group-item" id="date">Date: </li>
                <li className="list-group-item" id="location">Location:</li>
              </ul>
            </div>
            <a href="#BubbleVisual" id="link" className="btn btn-link">View More</a>
          </div>

          <div className="col-lg-9">
            <BubbleChart id="bubbleChart"
              graph= {{
                zoom: .65,
                offsetX: 0.15,
                offsetY: 0.05,
              }}
              showLegend={false} // optional value, pass false to disable the legend.
              legendPercentage={20} // number that represent the % of with that legend going to use.
              legendFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#000',
                    weight: 'bold',
                  }}
              valueFont={{
                    family: 'Arial',
                    size: 12,
                    color: '#fff',
                    weight: 'bold',
                  }}
              labelFont={{
                    family: 'Arial',
                    size: 16,
                    color: '#fff',
                    weight: 'normal',
                  }}
              data={data}
              bubbleClickFun={bubbleClick}
            />
          </div>

        </div>
      </div>
    );
  }
}
export default Bubble;
