import React, { Component } from 'react';
import './css/App.css';
import './css/landing-page.css';
import Navbar from './Navbar';
import About from './About';
import Home from './Home';

import States from './States';
import NaturalDisasters from './NaturalDisasters';
import Organizations from './Organizations';
import Visualizations from './Visualizations';
import Search from './Search';

import OrganizationInstance from './components/OrganizationInstance';
import StateInstance from './components/StateInstance';
import DisasterInstance from './components/DisasterInstance';

import NoMatch from './Error';
import ScrollBar from './Scrollbar'

import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
    <div className="App">
      <Navbar/>
      <ScrollBar/>
      <Router>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/about" component={About} />
          <Route exact path="/states/" component={States} />
          <Route exact path="/natural-disasters/" component={NaturalDisasters} />
          <Route exact path="/organizations/" component={Organizations} />
          <Route exact path="/visualizations/" component={Visualizations} />
          <Route exact path="/search/" component={Search} />

          <Route path="/states/:code" component={StateInstance} />
          <Route path="/natural-disasters/:disasternumber" component={DisasterInstance} />
          <Route path="/organizations/:ein" component={OrganizationInstance} />

          <Route path="*" component={NoMatch} />
        </Switch>
      </Router>
    </div>
    );
  }
}

export default App;
