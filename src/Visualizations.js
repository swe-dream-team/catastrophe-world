import React, { Component } from 'react';
import MapVisual from './MapVisual';
import Bubble from './Bubble';
import Footer from './components/Footer';
import LiquidGauge from './LiquidGauge';
import Scatterplot from './Scatterplot';
import BarChart from './BarChart';
import BarGraph from './BarGraph';
import './Visualizations.css';
import './css/style.css';

// Page that displays all visualizations
class Visualizations extends Component {

  render() {
    return (
      <div>

      <div className="container mb-5">
        {/* visuals for our models */}
        <div id="MapVisual">
          <h1 className="vis-header">Natural Disasters per State</h1>
          <div style={{marginLeft: "15vh"}}>
            <MapVisual/>
          </div>
        </div>
        <div id="BubbleVisual">
          <h1 className="vis-header">Recent FEMA funding of Natural Disasters</h1>
          <Bubble/>
        </div>
        <div id="BarChart">
          <h1>Total Organizations in each State</h1>
          <BarChart/>
        </div>

        {/* visuals for UnbEATable Food */}
        <div id="Customer">
          <h2>Customer Visualizations</h2>
          <div id="LiquidVisual">
            <h1 className="vis-header" style={{marginBottom:"4vh"}}>Percentage of Recipes with Certain Ingredients</h1>
            <LiquidGauge/>
          </div>
          <div id="LocationVisual">
            <h1 className="vis-header">Number of Food Items per Location</h1>
            <BarGraph/>
          </div>
          <div id="NutritionVisual" style={{overflowX: "hidden"}}>
            <h1 className="vis-header">Calorie v. Fat of Certain Foods</h1>
            <Scatterplot/>
          </div>
        </div>
      </div>

      <Footer/>
      </div>
    );
  }
}

export default Visualizations;
