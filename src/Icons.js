import React, { Component } from 'react';
import Footer from './components/Footer';

// Icons for bottom of landing page
class Icons extends Component {
  render() {
    return (
      <div>
      <section id="second" className="features-icons bg-white text-center">
        <h1>Explore our catastrophe relief data.</h1>
        <div className="container" style={{marginTop: '5%'}}>
          <div className="row">
            <div className="col-md-3 col-lg-3 box">
              <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3" style={{marginTop: '8%'}}>
                <div className="features-icons-icon d-flex">
                  <a className="icon m-auto" href="/states/"><i className="fas fa-globe-americas"></i></a>
                </div>
                <h3 style={{textTransform: 'none', fontSize: '3vh', marginBottom: '3vh' }}>States</h3>
                <p className="lead mb-0" style={{fontSize: '2.3vh'}}>See how different states are affected by natural disasters.</p>
              </div>
            </div>

            <div className="col-md-3 col-lg-3 box">
              <div className="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3" style={{marginTop: '8%'}}>
                <div className="features-icons-icon d-flex">
                    <a className="icon m-auto" href="/natural-disasters/"><i className="fas fa-fire-alt"></i></a>
                </div>
                <h3 style={{textTransform: 'none', fontSize: '3vh', marginBottom: '3vh' }}>Natural Disasters</h3>
                <p className="lead mb-0" style={{fontSize: '2.3vh'}}>Learn about the various kinds of natural disasters.</p>
              </div>
            </div>

            <div className="col-md-3 col-lg-3 box">
              <div className="features-icons-item mx-auto mb-0 mb-lg-3" style={{marginTop: '8%'}}>
                <div className="features-icons-icon d-flex">
                    <a className="icon m-auto" href="/organizations/"><i className="fas fa-hands-helping"></i></a>
                </div>
                <h3 style={{textTransform: 'none', fontSize: '3vh', marginBottom: '3vh' }}>Organizations</h3>
                <p className="lead mb-0" style={{fontSize: '2.3vh'}}>Get in touch with organizations dedicated on helping those affected.</p>
              </div>
            </div>

          </div>
        </div>
      </section>

      <Footer/>
      </div>
    );
  }
}

export default Icons;
