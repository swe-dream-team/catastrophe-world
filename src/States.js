import React, { Component } from 'react';
import axios from 'axios';
import Select from 'react-select';
import Pagination from './components/Pagination';
import StatesCard from './components/StatesCard';
import NumericalInput from './components/NumericalInput';
import Footer from './components/Footer';
import load from './img/final.gif';
import './css/States.css';

// States model page
class StatesPage extends Component {

per_page = 12;

sortOptions = [
  { value: '', label: 'Default' },
  { value: 'asc-name', label: "Name A-Z" },
  { value: 'dsc-name', label: 'Name Z-A' },
  { value: 'asc-population', label: 'Population \u2191' },
  { value: 'dsc-population', label: 'Population \u2193' },
  { value: 'asc-area', label: 'Area \u2191' },
  { value: 'dsc-area', label: 'Area \u2193' },
];

regionOptions = [
  { value: '', label: 'All Regions' },
  { value: 'West', label: "West" },
  { value: 'Midwest', label: 'Midwest' },
  { value: 'Southwest', label: 'Southwest' },
  { value: 'Northeast', label: 'Northeast' },
  { value: 'Southeast', label: 'Southeast' },
];

constructor(props) {
  super(props);
  this.state = {
      currentStates: 50,
      allStates: [],
      current: [],
      currentPage: 1,
      totalPages: null,
      filteredNum: 0,
      sortby: '',
      pop_min: '',
      pop_max: '',
      area_min: '',
      area_max: '',
      region: '',
      searchTerm: ''
  };
  this.onSort = this.onSort.bind(this);
  this.onFilter = this.onFilter.bind(this);
  this.onRegionFilter = this.onRegionFilter.bind(this);
  this.searchUpdated = this.searchUpdated.bind(this);
  this.updateSearch = this.updateSearch.bind(this);
}

buildUrl(currentPage){
  // let url = "http://localhost:5000/states/?page=" + currentPage
  let url = "https://api.catastrophe.world/states/?page=" + currentPage
  + "&per_page=" + this.per_page
  + "&sortby=" + this.state.sortby
  + "&pop_min=" + this.state.pop_min
  + "&pop_max=" + this.state.pop_max
  + "&area_min=" + this.state.area_min
  + "&area_max=" + this.state.area_max
  + "&region=" + this.state.region
  + "&searchTerm=" + this.state.searchTerm;

  // console.log(url)
  return url;
}

getTotalUrl(){
  let url = "https://api.catastrophe.world/states/?"
  + "sortby=" + this.state.sortby
  + "&pop_min=" + this.state.pop_min
  + "&pop_max=" + this.state.pop_max
  + "&area_min=" + this.state.area_min
  + "&area_max=" + this.state.area_max
  + "&region=" + this.state.region
  + "&searchTerm=" + this.state.searchTerm;

  // console.log(url)
  return url;
}

componentDidMount() {
  // let url = "http://localhost:5000/states/";
  // axios.get(url).then(response => {
  //    this.setState({ allStates: response.data});
  // });

  axios.get('https://api.catastrophe.world/states/').then(response => {
     this.setState({ allStates: response.data});
  });
}

onPageChanged = data => {
  const { currentPage, totalPages, pageOne, preview} = data;

  var curr;
  if (pageOne) {
    curr = 1;
  }
  else {
    curr = currentPage;
  }

  const url = this.buildUrl(curr) // always go to page 1 right?
  axios.get(url).then(response => {
    const current = response.data;
    if (preview) {
      this.setStates(current, this.state.searchTerm);
    }
    this.setState({ currentPage: curr, current, totalPages });
 });
}

setStates(states, term) {
  var stateCards = [];

  var len = states.length;

  var t = term;
  t = t.replace("+", " ");
  t = t.toLowerCase();

  var i;
  for (i = 0; i < len; i++) {
    states[i]["preview"] = this.stateContains(t, states[i]);
    stateCards.push(states[i]);
  }

  // console.log(stateCards);
  // this.setState({stateLoaded: true});
  this.setState({current: stateCards});
}

stateContains(term, state) {
  var name = state["name"].toLowerCase();
  var code = state["code"].toLowerCase();
  var region = state["region"].toLowerCase();

  if (name.includes(term)) {
    return false;
  }
  else if (code.includes(term)) {
    return false;
  }
  else if (region.includes(term)) {
    return false;
  }

  return true;
}

onSort(e) {
  this.setState(
      {sortby: e.value},
      function () {
          var data = this.state;
          data.pageOne = true;
          this.onPageChanged(data);
          this.onFilterChange();
      }
  )
}

onRegionFilter(f) {
  this.setState(
    {region: f.value},
    function () {
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
    }
  )
}

onFilterChange() {
  const url = this.getTotalUrl();
  axios.get(url).then(response => {
    const current = response.data;
    var currentStates = current.length;
    this.setState({ currentStates });
    // console.log(currentStates);
    // this.setState({ currentPage, current, totalPages });
    // console.log(totalPages);
 });
}

onFilter(val){
  const re = /^[0-9\b]+$/;

  var filter_values = {};
  var filter = 0;

  if (val.target.value === '' || re.test(val.target.value)) {
    filter = val.target.value;
 }

  if(val.target.id === "pop_min"){
    filter_values = {pop_min: filter}
  }
  else if(val.target.id === "pop_max"){
    filter_values = {pop_max: filter}
  }
  else if (val.target.id === "area_min"){
    filter_values = {area_min: filter}
  }
  else if (val.target.id === "area_max"){
    filter_values = {area_max: filter}
  }
  this.setState(
    filter_values,
    function() {
      var data = this.state;
      data.pageOne = true;
      this.onPageChanged(data)
      this.onFilterChange();
    }
  )
}

searchUpdated () {
  // term = document.getElementById('searchInput').value;
  // term = this.updatedSearch;
  this.setState(
    // {searchTerm: term.target.value},
    function (){
      // this.onPageChanged(this.state)
      // this.onFilterChange();
      var data = this.state;
      data.pageOne = true;
      data.preview = true;
      this.onPageChanged(data);
      this.onFilterChange();
    }
  )
}

updateSearch (term) {
  this.setState({searchTerm: term.target.value});
}

clearAll(){
  window.location.reload()
}

render() {
  const { allStates, current, currentPage, currentStates } = this.state;
  const totalStates = allStates.length;
  const pages = Math.ceil(currentStates/this.per_page);

  const curr = (pages === 0) ? 0 : currentPage;

  if (totalStates === 0) return (<div style={{background: '#FCFAFC', height: '100vh'}}>
    <img src={load} alt="loading" style={{marginTop: '5%', width: '20%'}}></img>
    </div>);

  const headerClass = ['text-dark py-2 pr-4 m-0', curr ? 'border-gray border-right' : ''].join(' ').trim();

  return (
    <div>

    <div className="container mb-0">
      <div className="row d-flex flex-row py-5">
        <div className="w-100 px-4 py-5 d-flex flex-row flex-wrap align-items-center justify-content-between">
          <div className="d-flex flex-row align-items-center">
            <h2 className={headerClass}>
              <strong className="text-secondary">{currentStates}</strong> States
            </h2>
            { curr && (
              <span className="current-page d-inline-block h-100 pl-4 text-secondary">
                Page <span className="font-weight-bold">{ curr }</span> / <span className="font-weight-bold">{ pages }</span>
              </span>
            ) }
          </div>
          <button type="button" className="btn btn-danger" onClick={this.clearAll}>Reset Filters</button>
        </div>

        {/* first row of filters */}
        <div className="row" id="filter-row-1">
          <div className="col-lg-9 col-md-9">
            <div style={{textAlign: 'left', fontSize: '120%', fontWeight: 'bold'}}>Filter Options</div>
            <table>
              <tbody>
                <NumericalInput
                  label="Population:"
                  lowId="pop_min"
                  highId="pop_max"
                  lowPlaceholder="Low"
                  highPlaceholder="High"
                  onChange={this.onFilter}
                />
                <NumericalInput
                  label="Area:"
                  lowId="area_min"
                  highId="area_max"
                  lowPlaceholder="Low"
                  highPlaceholder="High"
                  onChange={this.onFilter}
                />
              </tbody>
            </table>
          </div>

          <div className="col-lg-3 col-md-3">
            <Select name="Sort" placeholder="Sort Options" defaultValue="Region" id="sort-bar" options={this.sortOptions} onChange={this.onSort}/>
          </div>
        </div>

        {/* second row of filters */}
        <div className="row" id="filter-row-2">
          <div className="col-lg-2 col-md-4">
            <Select name="Region" placeholder="Region" defaultValue="" options={this.regionOptions} onChange={this.onRegionFilter}/>
          </div>

          <div className="col-lg-7 col-md-4"></div>

          <div className="col-lg-3 col-md-4">
            {/* <input type="text" placeholder="Search" onChange={this.searchUpdated} id="search-bar"/> */}
            {/* <form class="form-inline"> */}
            <input id='searchInput' className="form-control form-control-sm ml-0 w-75" type="text" placeholder="Search" aria-label="Search" style={{height: '37px', display: 'inline'}} onChange={this.updateSearch}>
            </input>
            <button type="submit" style={{border: 'none', background: 'none', padding: '0px', color: 'grey'}} onClick={this.searchUpdated}>
              <i className="fas fa-search" style={{display: 'inline', paddingLeft: '10px', fontSize: '20px'}}></i>
            </button>
            {/* <i class="fas fa-search"></i> */}
            {/* </form> */}
          </div>
        </div>

        {/* state instance cards */}
        <div className="row">
          { current.map(state => <StatesCard key={state.code} state={state} search={this.state.searchTerm.split(" ")} preview={state.preview} climate={state.climate}/>) }
        </div>

        {/* pagination */}
        <div className="d-flex flex-row py-4" style={{width: '100%', alignContent: 'center', textAlign: 'center', justifyContent: 'center'}}>
          <Pagination totalRecords={currentStates} pageLimit={12} pageNeighbours={1} onPageChanged={this.onPageChanged} currentPage={curr}/>
        </div>
        
      </div>
    </div>

    <Footer />
    </div>
  );
}
}

export default StatesPage;
