import React, { Component } from 'react';
import Icons from './Icons';
import DisasterCarousel from './Carousel';

class Home extends Component {
  render() {
    return (
      <div className="body-padding">
      <DisasterCarousel/>
      <Icons/>
      </div>
    );
  }

}

export default Home;