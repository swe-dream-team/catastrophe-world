import React, { Component } from 'react';
import * as d3 from "d3";
import './Scatterplot.css';

// Scatterplot visulization for UnbEATable Food
class Scatterplot extends Component {
    constructor(props) {
      super(props);
      this.state = {
            data: {}
      };
      this.drawChart = this.drawChart.bind(this);
    }

    drawChart() {
        d3.json('https://www.unbeatable-food.com:5000/api/food/search/').then(data => {
            // Variables
            var body = d3.select('#scatterplot')
              var margin = { top: 50, right: 50, bottom: 50, left: 50 }
              var h = 500 - margin.top - margin.bottom
              var w = 500 - margin.left - margin.right
              // Scales
            var colorScale = d3.scaleOrdinal(d3.schemeCategory10)
            var xScale = d3.scaleLinear()
              .domain([
                  d3.min([0,d3.min(data.foods,function (d) { return d.nutrients[0].amount })]),
                  d3.max([0,d3.max(data.foods,function (d) { return d.nutrients[0].amount })])
                  ])
              .range([0,w])
            var yScale = d3.scaleLinear()
              .domain([
                  d3.min([0,d3.min(data.foods,function (d) { return d.nutrients[1].amount })]),
                  d3.max([0,d3.max(data.foods,function (d) { return d.nutrients[1].amount })])
                  ])
              .range([h,0])
              // SVG
              var svg = body.append('svg')
                  .attr('height',h + margin.top + margin.bottom)
                  .attr('width',w + margin.left + margin.right)
                .append('g')
                  .attr('transform','translate(' + margin.left + ',' + margin.top + ')')
              // X-axis
              var xAxis = d3.axisBottom(xScale)
                .ticks(10)
            // Y-axis
              var yAxis = d3.axisLeft(yScale)
                .ticks(10)
            // Circles
            var circles = svg.selectAll('circle')
                .data(data.foods)
                .enter()
              .append('circle')
                .attr('cx',function (d) { return xScale(d.nutrients[0].amount) })
                .attr('cy',function (d) { return yScale(d.nutrients[1].amount) })
                .attr('r','10')
                .attr('stroke','black')
                .attr('stroke-width',1)
                .attr('fill',function (d,i) { return colorScale(i) })
                .on('mouseover', function () {
                  d3.select(this)
                    .transition()
                    .duration(500)
                    .attr('r',20)
                    .attr('stroke-width',3)
                })
                .on('mouseout', function () {
                  d3.select(this)
                    .transition()
                    .duration(500)
                    .attr('r',10)
                    .attr('stroke-width',1)
                })
              .append('title') // Tooltip
                .text(function (d) { return d.food.name +
                                     '\nCalories: ' + (d.nutrients[0].amount) + " cal" +
                                     '\nFat: ' + (d.nutrients[1].amount) + "g"})
            // X-axis
            svg.append('g')
                .attr('class','axis')
                .attr('transform', 'translate(0,' + h + ')')
                .call(xAxis)
              .append('text') // X-axis Label
                .attr('class','label')
                .attr('y',-17)
                .attr('x',w)
                .attr('dy','.71em')
                .style('fill','black')
                .text('Calories')
            // Y-axis
            svg.append('g')
                .attr('class', 'axis')
                .call(yAxis)
              .append('text') // y-axis Label
                .attr('class','label')
                .attr('transform','rotate(-90)')
                .attr('x',0)
                .attr('y',5)
                .attr('dy','.71em')
                .style('fill','black')
                .text('Fat (g)')
          })
    }

    componentDidMount() {
        this.drawChart();
    }

    render() {
      return (
        <div id="scatterplot"></div>
      );
    }
  }

export default Scatterplot;
