import React, { Component } from 'react';
import { Dropdown, DropdownItem, DropdownToggle, DropdownMenu } from 'reactstrap';
import './css/Navbar.css'

// navigation bar
class Navbar extends Component {

  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      dropdownOpen: false
    };
  }

  componentDidMount () {
    var prevPosition = window.pageYOffset;
    window.onscroll = function() {
      var currentPosition = window.pageYOffset;
      if (currentPosition <= 0) {
        document.getElementById("nav").style.top = "0";
        document.getElementById("nav").style.boxShadow = "";
      }
      else if (prevPosition > currentPosition) {
        document.getElementById("nav").style.top = "0";
        document.getElementById("nav").style.boxShadow = "0 5px 8px -1px #dddddd";
      } else {
        document.getElementById("nav").style.top = "-50px";
        document.getElementById("nav").style.boxShadow = "0 5px 8px -1px #dddddd";
      }
      prevPosition = currentPosition;
    }

    var path = window.location.pathname;
      if (path.includes('/states')) {
        if (document.getElementById('states')) {
          document.getElementById('states').style.color = '#2A78A9';
          document.getElementById('states').style.fontWeight = 'bold';
        }
      }
      else if (path.includes('/natural-disasters')) {
        if (document.getElementById('dis')) {
          document.getElementById('dis').style.color = '#2A78A9';
          document.getElementById('dis').style.fontWeight = 'bold';
        }
      }
      else if (path.includes('/organizations')) {
        if (document.getElementById('orgs')) {
          document.getElementById('orgs').style.color = '#2A78A9';
          document.getElementById('orgs').style.fontWeight = 'bold';
        }
      }
      else if (path.includes('/about')) {
        if (document.getElementById('abt')) {
          document.getElementById('abt').style.color = '#2A78A9';
          document.getElementById('abt').style.fontWeight = 'bold';
        }
      }
      else if (path.includes('/visualizations')) {
        if (document.getElementById('vis')) {
          document.getElementById('vis').style.color = '#2A78A9';
          document.getElementById('vis').style.fontWeight = 'bold';
        }
      }
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-white fixed-top scrolling-navbar" id="nav">
        {/* home icon */}
        <a className="navbar-brand" href="/">
          <img src="/img/favicon.png" width="30" height="30" className="d-inline-block align-top" alt=""/> catastrophe.world
        </a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <a className="nav-link" id="states" href="/states">States</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="dis" href="/natural-disasters">Natural Disasters</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="orgs" href="/organizations">Organizations</a>
            </li>

            {/* dropdown bar for visulizations */}
            <Dropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle} className="nav-item">
              <DropdownToggle nav caret id="vis">
                Visualizations
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem href="/visualizations#MapVisual" style={{fontSize: "90%"}}>
                  States Visual
                </DropdownItem>
                <DropdownItem href="/visualizations#BubbleVisual" style={{fontSize: "90%"}}>
                  Disasters Visual
                </DropdownItem>
                <DropdownItem href="/visualizations#BarChart" style={{fontSize: "90%"}}>
                  Organizations Visual
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem style={{fontSize: "90%"}} href="/visualizations#LiquidVisual">
                  UnbEATable Food Visual #1
                </DropdownItem>
                <DropdownItem style={{fontSize: "90%"}} href="/visualizations#LocationVisual">
                  UnbEATable Food Visual #2
                </DropdownItem>
                <DropdownItem style={{fontSize: "90%"}} href="/visualizations#NutritionVisual">
                  UnbEATable Food Visual #3
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>

            <li className="nav-item">
              <a className="nav-link" id="abt" href="/about">About</a>
            </li>
          </ul>

          {/* site-wide search bar */}
          <form className="form-inline my-2 my-lg-0" action="/search/"  method="GET">
            <input className="form-control mr-sm-2" type="text" name="search" placeholder="Search" aria-label="Search"></input>
            <button className="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
          </form>
          
        </div>
      </nav>
    );
  }
}

export default Navbar;
