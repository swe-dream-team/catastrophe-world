import React, { Component } from 'react';
import axios from 'axios';
import '../css/style.css';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import OSlider from './OrganizationSlider';
import NumberFormat from 'react-number-format';

library.add(fas)

class DisasterInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      disasternumber: props.match.params.disasternumber,
      attributes: null,
      isLoaded: false,
      error: null
    }

  }

  handle = (res) => {
    this.setState( {
        attributes: res
    });
}


  componentDidMount() {
    let url = 'https://api.catastrophe.world/natural-disasters/' + this.state.disasternumber;
    // let url = 'http://127.0.0.1:5000/natural-disasters/' + this.state.disasternumber;
    axios.get(url).then(response => {
      this.setState({attributes: response.data});
      this.setState({isLoaded: true});

      // dynamic get for organizations in this state
      let u = 'https://api.catastrophe.world/organizations/?stateorprovince=' + this.state.attributes['statecode'];
      // let u = 'http://localhost:5000/organizations/?stateorprovince=' + this.state.code;
      axios.get(u).then(response => {
        this.setState({organizations: response.data});
        this.setState({orgsLoaded: true});
      })
    })
  }

  render() {
    const { attributes, isLoaded, organizations, orgsLoaded} = this.state;

    if (isLoaded && orgsLoaded) {

      var statecode = attributes['statecode'];
      var orgs = JSON.stringify(organizations);

      return (
        <div className="container">
          <h2>{attributes.disastername}</h2>
          <h6><b>Date: </b>{attributes.declarationdate}</h6>
          <div className="row" style={{color: 'black'}}>
            <div className="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
              <div className="service-list">
                <div className="service-list-col1">
                  <FontAwesomeIcon icon="cloud-sun-rain" size="3x"></FontAwesomeIcon>
                </div>
                <div className="service-list-col2">
                  <h3>Incident Type</h3>
                  <p>{attributes.incidenttype}</p>
                </div>
              </div>
              <div className="service-list">
                <div className="service-list-col1">
                  <FontAwesomeIcon icon="search-location" size="3x"></FontAwesomeIcon>
                </div>
                <div className="service-list-col2">
                  <h3>Location</h3>
                  <p><a href={"/states/" + statecode}>{attributes.statename}</a></p>
                </div>
              </div>
              <div className="service-list">
                <div className="service-list-col1">
                  <FontAwesomeIcon icon="exclamation-circle" size="3x"></FontAwesomeIcon>
                </div>
                <div className="service-list-col2">
                  <h3>Classificaiton</h3>
                  <p>{attributes.declarationtype}</p>
                </div>
              </div>
              <div className="service-list">
                <div className="service-list-col1">
                  <FontAwesomeIcon icon="file-invoice-dollar" size="3x"></FontAwesomeIcon>
                </div>
                <div className="service-list-col2">
                  <h3>FEMA Funding for State</h3>
                  <p><NumberFormat value={attributes.obligatedfunding} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
                </div>
              </div>
              <div className="service-list">
                <div className="service-list-col1">
                  <FontAwesomeIcon icon="hand-holding-usd" size="3x"></FontAwesomeIcon>
                </div>
                <div className="service-list-col2">
                  <h3>FEMA Funding for Individuals</h3>
                  <p><NumberFormat value={attributes.approvedfunding} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
                </div>
              </div>
          </div>
          <figure className="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
            <img src={attributes.image} alt={attributes.name}></img>
            <p>{attributes.disastername} in {attributes.statename}</p>
          </figure>
        </div>
        <div className="instance-slider">
            <h5>Relief Organizations</h5>
            <OSlider data={orgs}/>
          </div>
        </div>
        );
    }
    return (<div></div>);
  }

}

export default DisasterInstance;
