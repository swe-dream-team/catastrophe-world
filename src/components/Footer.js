import React, { Component } from 'react';
import '../css/Footer.css';

class Footer extends Component {
  render() {
    return(
      <footer className="footer bg-light">
        <p>Thank you for visiting. We are truly honored by your presence on our site.</p>
      </footer>
    );
  }
}

export default Footer;
