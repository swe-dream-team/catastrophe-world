import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from "react-highlight-words";
import NumberFormat from 'react-number-format';

const highlightStyle = { backgroundColor: 'white', color: 'rgb(254, 208, 76)', padding: '0px'};

const StatesCard = props => {
  const { code: code = '', name = null, population = null, area = null, flag = null, region = null} = props.state || {};
  props.search[0] = props.search[0].replace("+", " ");
  const term = props.search[0].toLowerCase();
  var previewIt = props.preview;

  if (props.search[0] === "") {
    previewIt = false;
  } else {
    previewIt = true;
  }

  var button;
  if (previewIt) {
    var indexes = []
    var climate = props.climate.toLowerCase();

    var findTerm = function() {
      var climate = props.climate.toLowerCase();
      var i=-1;
      while((i=climate.indexOf(term,i+1)) >= 0) indexes.push(i);
    };
    findTerm();

    var previews = []
    var getPreviews = function() {
      var i;
      for (i = 0; i < indexes.length; i++) {
        var index = indexes[i];
        var start = (index - 100) > 0 ? index - 100: 0;
        var end = (index + 100) < climate.length ? index + 100 : climate.length;
        var prev = props.climate.slice(start, end);

        if (start > 0) {
          prev = "..." + prev;
        }
        if (end < climate.length) {
          prev = prev + "...";
        }

        previews.push(prev);
      }
    };
    getPreviews();

    // var inputText = props.climate;
    var highlighted = [];
    function highlight() {
      // if (index >= 0) { 
      //  innerHTML = innerHTML.substring(0,index) + "<span class='highlight'>" + innerHTML.substring(index,index+text.length) + "</span>" + innerHTML.substring(index + text.length);
      //  inputText.innerHTML = innerHTML;
      // }
      // var i;
      // for (i = 0; i < indexes.length; i++) {
      //   var index = indexes[i];
      //   inputText = inputText.substring(0,index) + (<mark style={{color: 'yellow'}}>) + inputText.substring(index,index+term.length) + (</mark>) + inputText.substring(index + climate.length);
      // }
      // console.log(inputText);
      var i;
      for (i = 0; i < previews.length; i++) {
        var prev = previews[i];
        var lower = prev.toLowerCase();
        var index;
        index = lower.indexOf(term);

        var beg = prev.substring(0, index);
        var searched = prev.substring(index, index+term.length);
        var end = prev.substring(index + term.length, prev.length);

        var hl = (<p style={{color: 'white', textAlign: 'left'}}>{beg}<mark style={{backgroundColor: 'transparent', color: 'rgb(254, 208, 76)', padding: '0px'}}>{searched}</mark>{end}</p>);
        highlighted.push(hl);
      }
    };
    highlight();

    button = (<a href={"/states/" + code} className="btn btn-outline-secondary state">View More
      <span>
        <h5 style={{color: 'white', textDecoration: 'underline'}}>Climate</h5>
        {/* {previews} */}
        { highlighted }
      </span>
    </a>);
  }
  else {
    button = (<a href={"/states/" + code} className="btn btn-outline-secondary">View More</a>);
  }

  return (
    <div className="col-sm-4" style={{marginBottom: '2.5%'}}>
      <div className="card">
          <div className="card h-10">
            <img className="img-thumbnail" src={flag} alt={name}
              style={{width: '90%', height: '230px', marginTop: '5%', marginLeft: '5%', objectFit: "scale-down"}}>
            </img>
            <div className="card-body">
              <h4 className="card-title"><font color="black">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={name}
              />
              </font></h4>
              <h6 className="card-subtitle mb-2 text-muted">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={code}
              />
              </h6>
              <h6 className="card-subtitle mb-2 text-muted">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={region}
              />
              </h6>
              <p className="card-subtitle"><b>Population:</b> <NumberFormat value={population} displayType={'text'} thousandSeparator={true}  /></p>
              <p className="card-subtitle"><b>Area:</b> <NumberFormat value={area} displayType={'text'} thousandSeparator={true}  /> sq. mi</p>
              <br></br>
              { button }
            </div>
        </div>
      </div>
    </div>
  )
}

StatesCard.propTypes = {
  state: PropTypes.shape({
    code: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    capital: PropTypes.string.isRequired,
    population: PropTypes.string.isRequired,
    area: PropTypes.string.isRequired,
    flag: PropTypes.string.isRequired,
    region: PropTypes.string
  }).isRequired
};

export default StatesCard;
