import React from 'react';
import Slider from 'react-slick';
var createReactClass = require('create-react-class');

var Article = createReactClass({
   render: function() {
       var image = this.props.data.image,
               title = this.props.data.charityname,
               subtitle = "View More";
       return (
           <figure className="snip1584">
               <img alt="charity" src={image} />
               <figcaption>
                   <h3>{title}</h3>
                   <h5>{subtitle}</h5>
               </figcaption><a href={"/organizations/" + this.props.data.ein}></a>
           </figure>
       )
   }
});

var News = createReactClass({
   render: function() {
       var data = this.props.data;
       var newsTemplate;
       var settings = {
            dots: true,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true
   }
       if (data.length > 0) {
           newsTemplate = data.map(function(item, index) {
               return (
                       <div key={index}>
                           <Article data={item} />
                       </div>
               )
           })
       } else {
           newsTemplate = <p>No data found</p>
       }
       return (
           <div className='news'>
               <div className="slider-wrapper">
               <Slider {...settings}>{newsTemplate}</Slider>
               </div>
               <strong className={'news__count ' + (data.length > 0 ? '':'none') }>
                   Total cards: {data.length}
               </strong>
           </div>
       );
   }
});

function OSlider(props) {
    var cards = JSON.parse(props.data);

    return (
        <div className='app'>
            <News data={cards} />
        </div>
    );
 }

export default OSlider;