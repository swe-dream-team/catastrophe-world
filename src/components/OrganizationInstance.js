import React, { Component } from 'react';
import axios from 'axios';
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { fas } from '@fortawesome/free-solid-svg-icons'
import "../css/style.css";
import DSlider from './DisasterSlider';

library.add(fas)

class OrganizationInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ein: props.match.params.ein,
      attributes: null,
      isLoaded: false,
      error: null
    }

  }

  handle = (res) => {
    this.setState( {
        attributes: res
    });
}


  componentDidMount() {
    let url = 'https://api.catastrophe.world/organizations/' + this.state.ein;
    // let url = "http://localhost:5000/organizations/" + this.state.ein;
    axios.get(url).then(response => {
      this.setState({attributes: response.data});
      this.setState({isLoaded: true});

      // dynamic get for disasters that happened in this state
      let ur = 'https://api.catastrophe.world/natural-disasters/?statecode=' + this.state.attributes['stateorprovince'];
      // let ur = 'http://localhost:5000/natural-disasters/?statecode=' + this.state.code;
      axios.get(ur).then(response => {
        this.setState({disasters: response.data});
        this.setState({disLoaded: true});
      })
    })
  }

  correctButton(link) {
    if (link.includes("facebook")) {
      return "fab fa-facebook-square";
    }
    else if (link.includes("twitter")) {
      return "fab fa-twitter-square";
    }
    else if (link.includes("youtube")) {
      return "fab fa-youtube-square";
    }
    else if (link.includes("flickr")) {
      return "fab fa-flickr";
    }
    else if (link.includes("instagram")) {
      return "fab fa-instagram";
    }
    else if (link.includes("linkedin")) {
      return "fab fa-linkedin";
    }
    else if (link.includes("pinterest")) {
      return "fab fa-pinterest-square";
    }
    else if (link.includes("tumblr")) {
      return "fab fa-tumblr-square";
    }
    else if (link.includes("google")) {
      return "fab fa-google-plus-square";
    }
    else if (link.includes("vimeo")) {
      return "fab fa-vimeo-square";
    }
    else {
      return "";
    }
  }

  mediaLinks(links) {
    var dict = {};
    var len = links.length;

    for (var i = 0; i < len; i++) {
      var l = links[i].replace(/[{}']+/g,'');
      var b = this.correctButton(l);
      dict[l] = b;
    }

    return dict;
  }

  render() {
    const { attributes, isLoaded, disasters, disLoaded } = this.state;


    if (isLoaded && disLoaded) {

      var dis = JSON.stringify(disasters);

      if (attributes.year === "") {
        attributes.year = "Unknown";
      }

      var links = attributes.social_media.split(",");
      var buttons = this.mediaLinks(links);
      var listItems;
      var none = "";

      if (buttons[""] === "") {
        listItems = "";
        none = "None";
      }
      else {
        listItems = Object.keys(buttons).map((link) =>
          <div className="col-sm-4"><a href={link} target="_blank" rel="noopener noreferrer"><i className={buttons[link]}></i></a></div>
        );
      }

      return (
        <div className="container">
        <h2>{attributes.charityname}</h2>
        <h6>{attributes.tagline}</h6>
        <div className="row">
          <div className="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
            <div className="service-list">
              <div className="service-list-col1">
                <i className="fas fa-location-arrow"></i>
              </div>
              <div className="service-list-col2">
                <h3>Headquarters</h3>
                <p><a href={"/states/" + attributes.stateorprovince}>{attributes.stateorprovince}</a></p>
              </div>
            </div>
            <div className="service-list">
              <div className="service-list-col1">
                <FontAwesomeIcon style={{color: 'black'}}icon="laptop-medical" size="3x"></FontAwesomeIcon>
              </div>
              <div className="service-list-col2">
                <h3>Contact</h3>
                <p><a href={attributes.websiteurl} target="_blank" rel="noopener noreferrer">{attributes.websiteurl}</a></p>
              </div>
            </div>
            <div className="service-list">
              <div className="service-list-col1">
                <FontAwesomeIcon style={{color: 'black'}}icon="hourglass-start" size="3x"></FontAwesomeIcon>
              </div>
              <div className="service-list-col2">
                <h3>Year Founded</h3>
                <p>{attributes.year}</p>
              </div>
            </div>
            <div className="service-list">
              <div className="service-list-col2">
                <h3>Social Media</h3>
                <div className="row" style={{width: '300px', marginRight: '2%'}}>
                  {listItems}
                  <p style={{marginLeft: "47%"}}>{none}</p>
                </div>
              </div>
            </div>
        </div>
        <figure className="col-lg-8 col-sm-6  fadeInUp delay-02s">
        <iframe title="video" width="730" height="530" allowFullScreen="allowfullscreen"
        frameBorder="0" src={attributes.video}>
        </iframe>
        </figure>
      </div>
      <div className="summary">
            <h5>Mission</h5>
            <p>{attributes.mission}</p>
      </div>
      <div className="instance-slider">
            <h5>Natural Disasters</h5>
            <DSlider data={dis}/>
          </div>
      </div>
      );
    }
    return (<div></div>);
  }

}

export default OrganizationInstance;
