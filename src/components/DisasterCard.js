import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from "react-highlight-words";
import '../css/style.css';
import NumberFormat from 'react-number-format';

const highlightStyle = { backgroundColor: 'white', color: 'rgb(254, 208, 76)', padding: '0px'}

const DisasterCard = props => {
  const { disasternumber: num = '', disastername = null, statename = null, incidenttype= null, obligatedfunding = null, declarationtype = null  } = props.disaster || {};

  return (
    <div className="col-sm-4" style={{marginBottom: '2.5%'}}>
      <div className="card" style={{height: '50vh', objectFit: 'scale-down'}}>
          <div className="card h-10" style={{height: '100%'}}>
            <div className="card-body" style={{height: '100%', objectFit: 'scale-down', paddingTop: '4%', paddingBottom: '0%'}}>
              <h4 className="card-title"><font color="black">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={disastername}
              />
              </font></h4>
              <h6 className="card-subtitle mb-2 text-muted">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={incidenttype}/>
              </h6>
              <p className="card-subtitle"><b>Afflicted Area: </b>
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={statename}/></p>
              <p className="card-subtitle"><b>Funding Given: </b><NumberFormat value={obligatedfunding} displayType={'text'} thousandSeparator={true} prefix={'$'} /></p>
              <p className="card-subtitle"><b>Classification:</b> {declarationtype}</p>
              <br></br>
            </div>
            <div className="card-button" style={{marginBottom: '2%'}}>
            <a href={"/natural-disasters/" + num} className="btn btn-outline-secondary"
                >View More</a>
            </div>
        </div>
      </div>
    </div>
  )
}

DisasterCard.propTypes = {
  disaster: PropTypes.shape({
    disasternumber: PropTypes.string.isRequired,
    disastername: PropTypes.string.isRequired,
    statename: PropTypes.string.isRequired,
    declarationdate: PropTypes.string.isRequired,
    incidenttype: PropTypes.string.isRequired,
    obligatedfunding: PropTypes.string.isRequired,
    declarationtype: PropTypes.string.isRequired
  }).isRequired
};

export default DisasterCard;