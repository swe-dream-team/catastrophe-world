import React, { Component } from 'react';
import axios from 'axios';
import '../css/style.css';
import OSlider from './OrganizationSlider';
import DSlider from './DisasterSlider';
import NumberFormat from 'react-number-format';


class StateInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      code: props.match.params.code,
      attributes: null,
      isLoaded: false,
      error: null
    }
  }

  handle = (res) => {
    this.setState( {
        attributes: res
    });
  }

  componentDidMount() {
    let url = 'https://api.catastrophe.world/states/' + this.state.code;
    // let url = 'http://localhost:5000/states/' + this.state.code;
    axios.get(url).then(response => {
      this.setState({attributes: response.data});
      this.setState({isLoaded: true});
    })

    // dynamic get for organizations in this state
    let u = 'https://api.catastrophe.world/organizations/?stateorprovince=' + this.state.code;
    // let u = 'http://localhost:5000/organizations/?stateorprovince=' + this.state.code;
    axios.get(u).then(response => {
      this.setState({organizations: response.data});
      this.setState({orgsLoaded: true});
    })

    // dynamic get for disasters that happened in this state
    let ur = 'https://api.catastrophe.world/natural-disasters/?statecode=' + this.state.code;
    // let ur = 'http://localhost:5000/natural-disasters/?statecode=' + this.state.code;
    axios.get(ur).then(response => {
      this.setState({disasters: response.data});
      this.setState({disLoaded: true});
    })
  }

  render() {
    const { attributes, isLoaded, organizations, orgsLoaded, disasters, disLoaded } = this.state;

    if (isLoaded && orgsLoaded && disLoaded) {

        var orgs = JSON.stringify(organizations);
        var dis = JSON.stringify(disasters);

        return (
          <div className="container">
            <h2>{attributes.name}</h2>
            <h6><b>Capital: </b>{attributes.capital}</h6>
            <div className="row">
              <div className="col-lg-4 col-sm-6 wow fadeInLeft delay-05s">
                <div className="service-list">
                  <div className="service-list-col1">
                    <i className="fas fa-home"></i>
                  </div>
                  <div className="service-list-col2">
                    <h3>Population (2018)</h3>
                    <p><NumberFormat value={attributes.population} displayType={'text'} thousandSeparator={true}  /> people</p>
                  </div>
                </div>
                <div className="service-list">
                  <div className="service-list-col1">
                    <i className="fas fa-chart-area"></i>
                  </div>
                  <div className="service-list-col2">
                    <h3>Area</h3>
                    <p><NumberFormat value={attributes.area} displayType={'text'} thousandSeparator={true}  /> sq. mi</p>
                  </div>
                </div>
                <div className="service-list">
                  <div className="service-list-col1">
                    <i className="fas fa-users"></i>
                  </div>
                  <div className="service-list-col2">
                    <h3>Population Density</h3>
                    <p><NumberFormat value={attributes.population_density} displayType={'text'} thousandSeparator={true}  /> ppl/sq. mi</p>
                  </div>
                </div>
                <div className="service-list">
                  <div className="service-list-col2">
                    <h3>Flag</h3>
                    <img style={{padding:'1px', border:'thin solid black'}} src={attributes.flag} alt={attributes.name} height="70%" width="70%"></img>
                  </div>
                </div>
              </div>
              <figure className="col-lg-8 col-sm-6  text-right wow fadeInUp delay-02s">
                <img src={attributes.map} alt={attributes.name}></img>
                <p>Map of {attributes.name} in the United States</p>
              </figure>
          </div>
          <div className="summary">
            <h5>Summary</h5>
            <p>{attributes.climate}</p>
          </div>
          <div className="instance-slider">
            <h5>Natural Disasters</h5>
            <DSlider data={dis}/>
          </div>
          <div className="instance-slider">
            <h5>Relief Organizations</h5>
            <OSlider data={orgs}/>
          </div>
        </div>
        );
    }
    return (<div></div>);
  }

}

export default StateInstance;
