import React, { Component } from 'react';

class NumericalInput extends Component {
    render() {
        const {label, lowId, highId, lowPlaceholder, highPlaceholder, onChange} = this.props;
        return (<tr>
            <td style={{padding: '0px, 30px, 0px, 0px'}}>{label}</td>

            <td><input className="form-control-sm" id={lowId} placeholder={lowPlaceholder}
                       type="text" onChange={onChange}
                       style={{maxWidth: '200px', display: 'inline-block', borderStyle: 'solid', borderWidth: '1px', borderColor: 'lightgrey'}}/></td>

            <td style={{padding: '0px, 20px, 0px, 20px'}}>to</td>

            <td><input className="form-control-sm" id={highId} placeholder={highPlaceholder}
                       type="text" onChange={onChange}
                       style={{maxWidth: '200px', display: 'inline-block', borderStyle: 'solid', borderWidth: '1px', borderColor: 'lightgrey'}}/></td>
        </tr>);
    }
}

export default NumericalInput;
