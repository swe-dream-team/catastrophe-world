import React from 'react';
import PropTypes from 'prop-types';
import Highlighter from "react-highlight-words";

const highlightStyle = { backgroundColor: 'white', color: 'rgb(254, 208, 76)', padding: '0px'}

const OrganizationCard = props => {
  const { ein: ein = '', charityname = null, stateorprovince = {}, tagline = null, image = null, rating = null, year = null } = props.organization || {};
  props.search[0] = props.search[0].replace("+", " ");
  const term = props.search[0];

  var previewIt = props.preview;

  if (props.search[0] === "") {
    previewIt = false;
  } else {
    previewIt = true;
  }

  var button;
  if (previewIt) {
    const lower = props.mission.toLowerCase();
    var index = lower.indexOf(term);

    var beg = props.mission.substring(0, index);
    var searched = props.mission.substring(index, index+term.length);
    var end = props.mission.substring(index + term.length, props.mission.length);

    var hl = (<p style={{color: 'white', textAlign: 'left'}}>{beg}<mark style={{backgroundColor: 'transparent', color: 'rgb(254, 208, 76)', padding: '0px'}}>{searched}</mark>{end}</p>);

    button = (<a href={"/organizations/" + ein} className="btn btn-outline-secondary org">View More
      <span style={{textAlign: 'left'}}>
        <h5 style={{color: 'white', textDecoration: 'underline'}}>Mission</h5>
        { hl }
      </span>
    </a>);
  }
  else {
    button = (<a href={"/organizations/" + ein} className="btn btn-outline-secondary">View More</a>);
  }

  return (
    <div className="col-sm-4" style={{marginBottom: '2.5%'}}>
      <div className="card" style={{height: '85vh', objectFit: 'scale-down'}}>
          <div className="card h-10" style={{height: '100%'}}>
            <img className="img-thumbnail" src={image} alt={charityname}
              style={{width: '90%', height: '45%', marginTop: '5%', marginLeft: '5%', objectFit: "scale-down"}}>
            </img>
            <div className="card-body" style={{paddingBottom: '0%'}}>
              <h4 className="card-title"><font color="black">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={charityname}
              />
              </font></h4>
              <h6 className="card-subtitle mb-2 text-muted">
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={tagline}
              />
              </h6>
              <p className="card-subtitle"><b>Headquarters: </b>
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={stateorprovince}
              />
              </p>
              {/* <a className="card-url" href={websiteurl}>
              <Highlighter
                  highlightStyle={highlightStyle}
                  searchWords={props.search}
                  autoEscape={true}
                  textToHighlight={websiteurl}
              />
              </a> */}
              <p className="card-subtitle"><b>Year Founded: </b> {year}</p>
              <p className="card-subtitle"><b>Rating: </b> {rating} &#9733;</p>
              <br></br>
            </div>
            <div className="card-button" style={{marginBottom: '2%'}}>
            { button }
            </div>
        </div>
      </div>
    </div>
  )
}

OrganizationCard.propTypes = {
  organization: PropTypes.shape({
    ein: PropTypes.string.isRequired,
    charityname: PropTypes.string.isRequired,
    stateorprovince: PropTypes.string.isRequired,
    tagline: PropTypes.string.isRequired,
    websiteurl: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    year: PropTypes.string,
    mission: PropTypes.string,
    social_media: PropTypes.string,
    video: PropTypes.string,
    rating: PropTypes.string
  }).isRequired
};

export default OrganizationCard;