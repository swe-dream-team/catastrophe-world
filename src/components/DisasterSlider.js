import React from 'react';
import Slider from 'react-slick';
var createReactClass = require('create-react-class');

var Article = createReactClass({
   render: function() {
       var name = this.props.data.disastername,
               date = this.props.data.declarationdate,
               type = this.props.data.incidenttype,
               subtitle = "View More";
       return (
           <figure className="snip1584" style={{border: '3px solid lightgray'}}>
               <h1>{name}</h1>
               <figcaption>
                   <h3 style={{overflowWrap: 'break-word'}}>{type}</h3>
                   <h3 style={{overflowWrap: 'break-word'}}>{date}</h3>
                   <h5>{subtitle}</h5>
               </figcaption><a href={"/natural-disasters/" + this.props.data.disasternumber}></a>
           </figure>
       )
   }
});

var News = createReactClass({
   render: function() {
       var data = this.props.data;
       var dots = true;
       if (data.length > 37) {
           dots = false;
       }
       var newsTemplate;
       var settings = {
            dots: dots,
            infinite: false,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true
       }

       if (data.length > 0) {
           newsTemplate = data.map(function(item, index) {
               return (
                       <div key={index}>
                           <Article data={item} />
                       </div>
               )
           })
       } else {
           newsTemplate = <p>No data found</p>
       }
       return (
           <div className='news'>
               <div className="slider-wrapper">
               <Slider {...settings}>{newsTemplate}</Slider>
               </div>
               <strong className={'news__count ' + (data.length > 0 ? '':'none') }>
                   Total cards: {data.length}
               </strong>
           </div>
       );
   }
});

function DSlider(props) {
    var cards = JSON.parse(props.data);

    return (
        <div className='app'>
            <News data={cards} />
        </div>
    );
 }

export default DSlider;