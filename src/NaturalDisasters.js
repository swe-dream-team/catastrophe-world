import React, { Component } from 'react';
import axios from 'axios';
import Select from 'react-select';
import Pagination from './components/Pagination';
import DisasterCard from './components/DisasterCard';
import Footer from './components/Footer';
import 'react-widgets/dist/css/react-widgets.css';
import load from './img/final.gif';
import './css/NaturalDisasters.css';

// Natural disasters model page
class NaturalDisasters extends Component {
  per_page = 12;

  states = [
    { value: '', label: 'All States' },
    { value: 'AL', label: 'Alabama' }, { value: 'AK', label: 'Alaska' }, { value: 'AZ', label: 'Arizona' },
    { value: 'AR', label: 'Arkansas' }, { value: 'CA', label: 'California' }, { value: 'CO', label: 'Colorado' },
    { value: 'CT', label: 'Connecticut' }, { value: 'DE', label: 'Delaware' }, { value: 'FL', label: 'Florida' },
    { value: 'GA', label: 'Georgia' }, { value: 'HI', label: 'Hawaii' }, { value: 'ID', label: 'Idaho' },
    { value: 'IL', label: 'Illinois' }, { value: 'IN', label: 'Indiana' }, { value: 'IA', label: 'Iowa' },
    { value: 'KS', label: 'Kansas' }, { value: 'KY', label: 'Kentucky' }, { value: 'LA', label: 'Louisiana' },
    { value: 'MI', label: 'Michigan' }, { value: 'MD', label: 'Maryland' }, { value: 'MA', label: 'Massachusetts' },
    { value: 'MO', label: 'Missouri' }, { value: 'MN', label: 'Minnesota' }, { value: 'MS', label: 'Mississippi' },
    { value: 'MT', label: 'Montana' }, { value: 'NE', label: 'Nebraska' }, { value: 'NV', label: 'Nevada' },
    { value: 'NH', label: 'New Hampshire' }, { value: 'NJ', label: 'New Jersey' }, { value: 'NM', label: 'New Mexico' },
    { value: 'NY', label: 'New York' }, { value: 'NC', label: 'North Carolina' }, { value: 'ND', label: 'North Dakota' },
    { value: 'OH', label: 'Ohio' }, { value: 'OK', label: 'Oklahoma' }, { value: 'OR', label: 'Oregon' },
    { value: 'PA', label: 'Pennsylvania' }, { value: 'RI', label: 'Rhode Island' }, { value: 'SC', label: 'South Carolina' },
    { value: 'SD', label: 'South Dakota' }, { value: 'TN', label: 'Tennessee' }, { value: 'TX', label: 'Texas' },
    { value: 'UT', label: 'Utah' }, { value: 'VT', label: 'Vermont' }, { value: 'VA', label: 'Virginia' },
    { value: 'WA', label: 'Washington' }, { value: 'WV', label: 'West Virginia' }, { value: 'WI', label: 'Wisconsin' },
    { value: 'WY', label: 'Wyoming' }
  ];

  incidents = [
    {value: "", label: "All Incidents"},
    {value: "Severe Storm(s)", label: "Severe Storm(s)"},
    {value: "Mud/Landslide", label: "Mud/Landslide"},
    {value: "Dam/Levee Break", label: "Dam/Levee Break"},
    {value: "Volcano", label: "Volcano"},
    {value: "Fire", label: "Fire"},
    {value: "Hurricane", label: "Hurricane"},
    {value: "Terrorist", label: "Terrorist"},
    {value: "Chemical", label: "Chemical"},
    {value: "Toxic Substances", label: "Toxic Substances"},
    {value: "Freezing", label: "Freezing"},
    {value: "Severe Ice Storm", label: "Severe Ice Storm"},
    {value: "Earthquake", label: "Earthquake"},
    {value: "Flood", label: "Flood"},
    {value: "Snow", label: "Snow"},
    {value: "Tornado", label: "Tornado"},
    {value: "Coastal Storm", label: "Coastal Storm"},
    {value: "Other", label: "Other"}
  ];

  sortOptions = [
    { value: '', label: 'Default' },
    { value: 'asc-state', label: 'State A-Z' },
    { value: 'dsc-state', label: 'State Z-A' },
    { value: 'asc-type', label: 'Type A-Z' },
    { value: 'dsc-type', label: 'Type Z-A' },
  ];

  constructor(props) {
    super(props);
    this.state = {
        currentDisasters: 645,
        allDisasters: [],
        current: [],
        currentPage: 1,
        totalPages: null,
        sortby: '',
        funding: '',
        location: '',
        incident: '',
        searchTerm: ''
    };
    this.onSort = this.onSort.bind(this);
    this.onNumericFilter = this.onNumericFilter.bind(this);
    this.onFilter = this.onFilter.bind(this);
    this.onIncidentFilter = this.onIncidentFilter.bind(this);
    this.searchUpdated = this.searchUpdated.bind(this);
  }

  buildUrl(currentPage){
    // let url = "http://localhost:5000/natural-disasters/?page=" + currentPage
    let url = "https://api.catastrophe.world/natural-disasters/?page=" + currentPage
    + "&per_page=" + this.per_page
    + "&sortby=" + this.state.sortby
    + "&funding=" + this.state.funding
    + "&incident=" + this.state.incident
    + "&statecode=" + this.state.location
    + "&searchTerm=" + this.state.searchTerm;

    // console.log(url)
    return url;
  }

  getTotalUrl(){
    // let url = "http://localhost:5000/natural-disasters/?page=" + currentPage
    let url = "https://api.catastrophe.world/natural-disasters/?page=1"
    + "&per_page=" + 650
    + "&sortby=" + this.state.sortby
    + "&funding=" + this.state.funding
    + "&incident=" + this.state.incident
    + "&statecode=" + this.state.location
    + "&searchTerm=" + this.state.searchTerm;

    // console.log(url)
    return url;
  }

  componentDidMount() {
    // axios.get('http://localhost:5000/natural-disasters/?page=1&per_page=1000').then(response => {
    //    this.setState({ allDisasters: response.data});
    // });

    axios.get('https://api.catastrophe.world/natural-disasters/?page=1&per_page=1000').then(response => {
       this.setState({ allDisasters: response.data});
    });
  }

  onPageChanged = data => {
    // const { currentPage, totalPages} = data;
    // const url = this.buildUrl(currentPage)
    const { currentPage, totalPages, pageOne} = data;

    var curr;
    if (pageOne) {
      curr = 1;
    }
    else {
      curr = currentPage;
    }

    const url = this.buildUrl(curr)

    axios.get(url).then(response => {
      const current = response.data;
      //  this.setState({ currentPage, current, totalPages });
      this.setState({ currentPage: curr, current, totalPages });
    });
  }

  onSort(e) {
    this.setState(
        {sortby: e.value},
        function () {
            // this.onPageChanged(this.state)
            var data = this.state;
            data.pageOne = true;
            this.onPageChanged(data);
            this.onFilterChange();
        }
    )
  }

  onNumericFilter(val){
    const re = /^[0-9\b]+$/;

    var filter_values = {};
    var filter = 0;

    if (val.target.value === '' || re.test(val.target.value)) {
      filter = val.target.value;
   }
    console.log(filter);
    if(val.target.id === "funding"){
      filter_values = {funding: filter}
    }
    this.setState(
      filter_values,
      function() {
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  onFilterChange() {
    const url = this.getTotalUrl();
    axios.get(url).then(response => {
      const current = response.data;
      var currentDisasters = current.length;
      this.setState({ currentDisasters });
      // console.log(currentStates);
      // this.setState({ currentPage, current, totalPages });
      // console.log(totalPages);
   });
  }


  onFilter(val){
    var filter_values = {};
    filter_values = {location: val.value};
    this.setState(
      filter_values,
      function() {
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  onIncidentFilter(e){
    var filter_values = {};
    if (e === "All Incidents"){
      filter_values = {incident: ''};
    }
    else {
      filter_values = {incident: e.value};
    }
    this.setState(
      filter_values,
      function() {
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  searchUpdated (term) {
    this.setState(
      {searchTerm: term.target.value},
      function (){
        // this.onPageChanged(this.state)
        var data = this.state;
        data.pageOne = true;
        this.onPageChanged(data);
        this.onFilterChange();
      }
    )
  }

  clearAll(){
    window.location.reload()
  }

  render() {
    const { allDisasters, current, currentPage, currentDisasters } = this.state;
    const totalDisasters = allDisasters.length;

    const pages = Math.ceil(currentDisasters/this.per_page);
    const curr = (pages === 0) ? 0 : currentPage;

    if (totalDisasters === 0) return (<div style={{background: '#FCFAFC', height: '100vh'}}>
      <img src={load} alt="loading" style={{marginTop: '5%', width: '20%'}}></img>
      </div>);

    const headerClass = ['text-dark py-2 pr-4 m-0', curr ? 'border-gray border-right' : ''].join(' ').trim();

    return (
      <div>

      <div className="container mb-5">
        <div className="row d-flex flex-row py-5">
          <div className="w-100 px-4 py-5 d-flex flex-row flex-wrap align-items-center justify-content-between">
            <div className="d-flex flex-row align-items-center">
              <h2 className={headerClass}>
                <strong className="text-secondary">{currentDisasters}</strong> Disasters
              </h2>
              { curr && (
                <span className="current-page d-inline-block h-100 pl-4 text-secondary">
                  Page <span className="font-weight-bold">{ curr }</span> / <span className="font-weight-bold">{ pages }</span>
                </span>
              ) }
            </div>
            <button type="button" className="btn btn-danger" onClick={this.clearAll}>Reset Filters</button>
          </div>

          {/* first row of filters */}
          <div className="row" id="filter-row-1">
            <div className="col-lg-6 col-md-6">
              <div style={{textAlign: 'left', fontSize: '120%', fontWeight: 'bold'}}>Filter Options</div>
              <table>
                <tbody>
                  <tr>
                    <td style={{padding: '0px, 30px, 0px, 0px'}}>Minimum FEMA funding:</td>
                    <td><input className="form-control-sm" id='funding' placeholder='Amount' type="text" onChange={this.onNumericFilter} style={{maxWidth: '200px', display: 'inline-block', borderStyle: 'solid', borderWidth: '1px', borderColor: 'lightgrey'}}/></td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div className="col-lg-4 col-md-2"></div>

            <div className="col-lg-2 col-md-4">
              <Select name="Sort" placeholder="Sort Options" defaultValue="" id="sort-bar"
                      options={this.sortOptions} onChange={this.onSort}/>
            </div>
          </div>

          {/* second row of filters */}
          <div className="row" id="filter-row-2">
            <div className="col-lg-2 col-md-3">
              <Select name="State Filter" placeholder="State" defaultValue=""
                      options={this.states} onChange={this.onFilter}/>
            </div>
            <div className="col-lg-2 col-md-3">
              <Select name="Incident Filter" placeholder="Incident Type" defaultValue=""
                      options={this.incidents} onChange={this.onIncidentFilter}/>
            </div>
            <div className="col-lg-5 col-md-2"></div>
            <div className="col-lg-3 col-md-4">
              <input type="text" placeholder="Search" onChange={this.searchUpdated} id="search-bar"/>
            </div>
          </div>
          { current.map(disaster => <DisasterCard key={disaster.disasternumber} disaster={disaster} search={this.state.searchTerm.split(" ")}/>) }
        </div>

        {/* pagination */}
        <div className="d-flex flex-row py-4" style={{width: '100%', alignContent: 'center', textAlign: 'center', justifyContent: 'center'}}>
          <Pagination totalRecords={currentDisasters} pageLimit={12} pageNeighbours={0} onPageChanged={this.onPageChanged} currentPage={curr}/>
        </div>
      </div>

      <Footer/>
      </div>
    );
  }
}

export default NaturalDisasters;
