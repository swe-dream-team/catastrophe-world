import React, { Component } from 'react';
import axios from 'axios';

class GitLabStats extends Component {

  constructor(props) {
    super(props)
    this.state = {
      gitlab: {},
      people: ['chandlerjyoon@gmail.com', 'prestonbao@utexas.edu', 'wangallen98@gmail.com', 'david.b.shiu@gmail.com', 'mistika10@gmail.com'],
      names: ['Chandler Yoon', 'Preston Bao', 'Allen Wang', 'David Shiu', 'Alex Leung'],
      commits: new Map(),
      issues: new Map(),
      testCount: [0,0,0,0,0],
    };
  }

  load_gitlab_commits = (pageNum) => {
    const gitlabUrlCommits =  "https://gitlab.com/api/v4/projects/11101455/repository/commits?per_page=100&page=" + pageNum;
    axios({
      url: gitlabUrlCommits,
      method: 'get',
    })
    .then((response) => {
      if (response === undefined | response.data.length ===0) {
        return;
      }

      var commitCall = new Map();
      if (pageNum === 1) {
        for (var i=0; i<this.state.people.length; i++) {
          commitCall.set(this.state.people[i], 0)
        }
      } else {
        commitCall = this.state.commits;
      }

      for (var index = 0; index < response.data.length; index++) {
        var person = response.data[index].committer_email;
        if (commitCall.has(person)) {
          var numCommit = commitCall.get(person);
          commitCall.set(person, numCommit+1)
        }
      }

      this.setState({commits: commitCall})

      if (response.data.length === 100) {
        this.load_gitlab_commits(pageNum+1)
      }
    })
    .catch((response) => {
      console.log('Getting commit statistics failed');
    });
}

load_gitlab_issues(pageNum) {
  const gitlabUrlIssues =  "https://gitlab.com/api/v4/projects/11101455/issues?per_page=100&page=" + pageNum;
    axios({
      url: gitlabUrlIssues,
      method: 'get',
    })
    .then((response) => {
      if (response === undefined | response.data.length ===0) {
        return;
      }

      var issueCall = new Map();
      if (pageNum === 1) {
        for (var i=0; i<this.state.names.length; i++) {
          issueCall.set(this.state.names[i], 0)
        }
      } else {
        issueCall = this.state.issues;
      }

      for (var ind = 0; ind < response.data.length; ind++) {
        var name = response.data[ind].author.name;
        if (issueCall.has(name)) {
          var numIssue = issueCall.get(name);
          issueCall.set(name, numIssue+1)
        }
      }

      this.setState({issues: issueCall})

      if (response.data.length === 100) {
        this.load_gitlab_issues(pageNum+1)
      }
    })
    .catch((response) => {
      console.log('Getting issue statistics failed');
    });
}

load_gitlab_tests() {
  var testCount = [0, 0, 0, 0, 0];
  const urls = ["https://gitlab.com/api/v4/projects/11101455/repository/files/backend%2ftests.py/raw?ref=master",
                  "https://gitlab.com/api/v4/projects/11101455/repository/files/test%2fguitests.py/raw?ref=master",
                  "https://gitlab.com/api/v4/projects/11101455/repository/files/src%2fApp.test.js/raw?ref=master",
                  ];
  urls.forEach((url) => {
    axios.get(url).then(res => {
      const lines = res.data.split("\n");
      for (var i = 0; i < 5; i++) {
        const current = lines[i].split(" ");
        switch (current[1]) {
          case ("Chandler:"):
            testCount[0] += parseInt(current[2]);
            break;
          case ("Preston:"):
            testCount[1] += parseInt(current[2]);
            break;
          case ("Allen:"):
            testCount[2] += parseInt(current[2]);
            break;
          case ("David:"):
            testCount[3] += parseInt(current[2]);
            break;
          case ("Alex:"):
            testCount[4] += parseInt(current[2]);
            break;
          default:
        }
      }
      this.setState({testCount: testCount});
    });
  });
}

async componentWillMount() {
  this.load_gitlab_commits(1);
  this.load_gitlab_issues(1);
  this.load_gitlab_tests();
}

render() {
  var totalCommits = 0;
  var totalIssues = 0;
  var totalTests = 0;

  var people = this.state.people;
  var names = this.state.names;
  for (var i = 0; i < people.length; i++){
    var person = people[i]
    var name = names[i]
    totalCommits += this.state.commits.get(person);
    totalIssues += this.state.issues.get(name);
    totalTests += this.state.testCount[i]
  }
  totalCommits = totalCommits.toString();
  totalIssues = totalIssues.toString();
  return (
    <div className="table-container">
      <h2 className="infotitles">GitLab Stats</h2>
      <table className="table table-striped bg-light">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Commits</th>
            <th scope="col">Issues</th>
            <th scope="col">Unit Tests</th>
          </tr>
        </thead>
        <tbody>
        <tr>
          <th>{names[0]}</th>
          <td>{this.state.commits.get(people[0])}</td>
          <td>{this.state.issues.get(names[0])}</td>
          <td>{this.state.testCount[0]}</td>
        </tr>
        <tr>
          <th>{names[1]}</th>
          <td>{this.state.commits.get(people[1])}</td>
          <td>{this.state.issues.get(names[1])}</td>
          <td>{this.state.testCount[1]}</td>
        </tr>
        <tr>
          <th>{names[2]}</th>
          <td>{this.state.commits.get(people[2])}</td>
          <td>{this.state.issues.get(names[2])}</td>
          <td>{this.state.testCount[2]}</td>
        </tr>
        <tr>
          <th>{names[3]}</th>
          <td>{this.state.commits.get(people[3])}</td>
          <td>{this.state.issues.get(names[3])}</td>
          <td>{this.state.testCount[3]}</td>
        </tr>
        <tr>
          <th>{names[4]}</th>
          <td>{this.state.commits.get(people[4])}</td>
          <td>{this.state.issues.get(names[4])}</td>
          <td>{this.state.testCount[4]}</td>
        </tr>
        <tr>
          <th>Total</th>
          <th>{totalCommits}</th>
          <th>{totalIssues}</th>
          <th>{totalTests}</th>
        </tr>
        </tbody>
      </table>
    </div>
    );
  }
}

export default GitLabStats;
