import React, { Component } from "react";
import "./css/Error.css";

// Error page 
class Error extends Component {
  render() {
    return (
      <div className="text-center">
        <h1 className="display-1 mt-2 title"> 404 </h1>
        <img className="skull" src = "/img/skull.png" alt="skull"></img>
        <div className="message"> The page you were looking for either moved or doesn't exist </div>
      </div>
    )
  }
}

export default Error;
