# Chandler: 18
# Preston: 0
# Allen: 11
# David: 0
# Alex: 14

from unittest import main, TestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


class test(TestCase):
    def setUp(self):
        # Google Chrome
        self.driver = webdriver.Chrome("./chromedriver.exe")

    # Alex
    def test_1(self):
        self.driver.get("https://www.catastrophe.world")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Alex
    def test_2(self):
        self.driver.get("https://www.catastrophe.world/states")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Alex
    def test_3(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Alex
    def test_4(self):
        self.driver.get("https://www.catastrophe.world/organizations")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Alex
    def test_5(self):
        self.driver.get("https://www.catastrophe.world/about")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Alex
    def test_6(self):
        self.driver.get("https://www.catastrophe.world")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Alex
    def test_7(self):
        self.driver.get("https://www.catastrophe.world/states")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Alex
    def test_8(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Alex
    def test_9(self):
        self.driver.get("https://www.catastrophe.world/organizations")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Alex
    def test_10(self):
        self.driver.get("https://www.catastrophe.world/about")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_11(self):
        self.driver.get("https://www.catastrophe.world/states/AL")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_12(self):
        self.driver.get("https://www.catastrophe.world/states/IL")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_13(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/4101")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_14(self):
        self.driver.get("https://www.catastrophe.world/organizations/94-3088881")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_15(self):
        self.driver.get("https://www.catastrophe.world/states/IL")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_16(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/4101")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_17(self):
        self.driver.get("https://www.catastrophe.world/organizations/94-3088881")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_18(self):
        self.driver.get("https://www.catastrophe.world/states/WA")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_19(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/4308")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_20(self):
        self.driver.get("https://www.catastrophe.world/organizations/58-1535414")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_21(self):
        self.driver.get("https://www.catastrophe.world/states/IA")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_22(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/3375")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_23(self):
        self.driver.get("https://www.catastrophe.world/organizations/53-0196605")
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Chandler
    def test_24(self):
        self.driver.get("https://www.catastrophe.world/states/WA")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_25(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/4308")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_26(self):
        self.driver.get("https://www.catastrophe.world/organizations/58-1535414")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_27(self):
        self.driver.get("https://www.catastrophe.world/states/IA")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_28(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/3375")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_29(self):
        self.driver.get("https://www.catastrophe.world/organizations/53-0196605")
        self.assertEqual(self.driver.title, "catastrophe.world")

        # Chandler

    def test_30(self):
        self.driver.get("https://www.catastrophe.world/states/AL")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_31(self):
        self.driver.get("https://www.catastrophe.world/natural-disasters/4253")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Chandler
    def test_32(self):
        self.driver.get("https://www.catastrophe.world/organizations/58-1376648")
        self.assertEqual(self.driver.title, "catastrophe.world")

    # Allen
    def test_33(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/about/")
        self.assertEqual(driver.find_element_by_id("purpose").text, "Purpose")

    # Allen
    def test_34(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/about/")
        self.assertEqual(driver.find_element_by_tag_name("h5").text, "Preston Bao")

    # Allen
    def test_35(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/about/")
        temp = driver.find_element_by_xpath(
            "/html/body/div/div/div/section/div/h2"
        ).text
        self.assertEqual(temp, "Purpose")

    # Allen
    def test_36(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/about/")
        temp = driver.find_element_by_xpath(
            "/html/body/div/div/div/section[2]/div/h2"
        ).text
        self.assertEqual(temp, "Meet the Team")

    # Allen
    def test_37(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/states/")
        temp = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(temp, "50")

    # Allen
    # search test
    def test_38(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/states/")
        search = driver.find_element_by_id("search-bar")
        search.send_keys("texas")
        time.sleep(2)
        numStates = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(numStates, "2")

    # Allen
    # search test
    def test_39(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/states/")
        search = driver.find_element_by_id("search-bar")
        search.send_keys("food")
        time.sleep(2)
        numStates = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(numStates, "0")

    # Allen
    # search test
    def test_40(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/states/")
        search = driver.find_element_by_id("search-bar")
        search.send_keys("new york")
        time.sleep(2)
        numStates = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(numStates, "2")

    # Allen
    # search test
    def test_41(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/natural-disasters/")
        search = driver.find_element_by_id("search-bar")
        search.send_keys("storm")
        time.sleep(5)
        numReturned = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(numReturned, "293")

    # Allen
    # filter test
    def test_42(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/natural-disasters/")
        sort = driver.find_element_by_id("funding")
        sort.send_keys("1")
        time.sleep(2)
        numReturned = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(numReturned, "545")

    # Allen
    # filter test
    def test_43(self):
        driver = self.driver
        driver.get("https://www.catastrophe.world/natural-disasters/")
        sort = driver.find_element_by_id("funding")
        sort.send_keys("999999")
        time.sleep(2)
        numReturned = driver.find_element_by_xpath(
            "/html/body/div/div/div/div/div/div/h2/strong"
        ).text
        self.assertEqual(numReturned, "383")

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    main()
